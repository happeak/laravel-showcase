# Happeak Showcase
This is a layout project for any Happeak Store. 

Package is compatible only with Laravel framework. 

**Quick start**
1. Create new Laravel application. 
2. Add 'happeak/laravel-showcase' to the require section of composer.json or run
```bash
composer require happeak/laravel-showcase
```
Package's composer json use extra section for automatically inject it's service providers to project container so you don't need to require it in any place. It works out from the box.

Then run 
```bash
php artisan vendor:publish --provider=Happeak\Showcase\Providers\ShowcaseServiceProvider
```
or 
```bash
php artisan vendor:publish --provider=Happeak\Showcase\Providers\ShowcaseServiceProvider --tag=config
```
where 'config' can be replaced with 'views', 'lang' or 'public' to publish only the needed parts of the package.

Set up your .env file: filling database parameters, shop key and secret and Ecom settings is required. But for the last one there are some defaults. 

Run 
```bash
php artisan migrate
```
to create a database schema from migrations. 

Now you can synchronize shop entities with happeak.ru. You can use special console commands available from the box: 
```bash
php artisan sync:brands
php artisan sync:categories
php artisan sync:models
php artisan sync:products
php artisan sync:properties {--type=[models,products]}
```

**Give it a try!**
Run ```php artisan serve``` command or create a virtual host for your project. 
And visit /catalog/{category_slug} page. You will see a list of products.

Now you a free to extend your store.

Good luck!