<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 * @package App
 *
 * @property int $id
 * @property string $url
 * @property string $image
 * @property string $image_mobile
 * @property string $title
 * @property bool $is_enabled
 */
class Banner extends Model
{

    protected $table = 'shop_banners';

    protected $casts = [
        'is_enabled' => 'boolean',
    ];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return null|string
     */
    public function getImageMobile(): ?string
    {
        return $this->image_mobile;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->is_enabled;
    }
}
