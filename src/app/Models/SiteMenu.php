<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteMenu
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property SiteMenuItem[]|Collection $items
 */
class SiteMenu extends Model
{

    protected $table = 'shop_menus';

    protected $casts = [];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function hasItems(): bool
    {
        return $this->getItems()->isNotEmpty();
    }

    /**
     * @return bool
     */
    public function hasNotItems(): bool
    {
        return !$this->hasItems();
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(SiteMenuItem::class, 'menu_id', 'id');
    }
}
