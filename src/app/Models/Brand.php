<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 * @package Happeak\Showcase\Models
 *
 * @property int $id
 * @property string $name
 */
class Brand extends Model
{

    protected $table = 'shop_brands';

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
