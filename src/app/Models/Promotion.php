<?php

namespace Happeak\Showcase\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotion
 * @package App\Models\Shop
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property array $products
 * @property boolean $is_public
 * @property boolean $only_available
 * @property Carbon $start_at
 * @property Carbon $expire_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Promotion extends Model
{

    /**
     * @var string
     */
    protected $table = 'shop_promotions';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'is_public'      => 'boolean',
        'only_available' => 'boolean',
        'products'       => 'array',
    ];

    protected $dates = [
        'start_at',
        'expire_at',
    ];

    protected static function boot()
    {
        parent::boot();

        // We need only published and unblocked products
        static::addGlobalScope('is_public', function (Builder $builder) {
            $builder->where('is_public', true);
        });
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return Carbon
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @return bool
     */
    public function hasExpirationDate(): bool
    {
        return !is_null($this->getExpireAt());
    }

    /**
     * @return array
     */
    public function getProducts(): ?array
    {
        return $this->products;
    }

    /**
     * @return bool
     */
    public function hasProducts(): bool
    {
        return !empty($this->getProducts());
    }

    /**
     * @return bool|null
     */
    public function getOnlyAvailable(): ?bool
    {
        return $this->only_available;
    }
}
