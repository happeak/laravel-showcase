<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\Property
 *
 * @property int $id
 * @property int $category_id
 * @property string $destination
 * @property string|null $unit
 * @property int $position
 * @property string|null $pattern
 * @property int $is_filter
 * @property string $value_type
 * @property string|null $default_value
 * @property string|null $group
 * @property string $name
 * @property-read Category $category
 * @property-read PropertyValue[] $values
 * @property int|null $brand_id
 */
class Property extends Model
{

    const PROPERTY_MODEL_COLOR   = 588;
    const PROPERTY_PRODUCT_COLOR = 591;
    const PROPERTY_PRODUCT_SIZE  = 587;
    const PROPERTY_MODEL_AGE     = 0;
    const PROPERTY_MODEL_SEX     = 0;

    /**
     * @var string $table
     */
    protected $table = 'shop_properties';

    /**
     * @var array $guarded
     */
    protected $guarded = [];

    /**
     * @var array $casts
     */
    protected $casts = [
        'values' => 'array',
    ];
}
