<?php

namespace Happeak\Showcase\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package Happeak\Showcase\Models
 *
 * @property int $id
 * @property string $name
 * @property string $name_brand
 * @property string $name_model
 * @property string $name_type
 * @property string $name_type_gen
 * @property string $name_title
 * @property string $name_group
 * @property string $name_color
 * @property string $slug
 * @property string $description
 *
 * @property array $images
 *
 * @property float $base_price
 * @property float $price
 * @property int $stock_count
 * @property Carbon $stock_date
 *
 * @property boolean $is_public
 * @property boolean $is_blocked
 *
 * @property int $brand_id
 * @property int $category_id
 * @property int $subcategory_id
 * @property int $model_id
 *
 * @property Carbon $synced_at
 *
 * @property Brand $brand
 * @property Category $category
 * @property Category $subcategory
 * @property ProductModel $model
 * @property PropertyValue $property_values
 */
class Product extends Model
{

    protected $table = 'shop_products';

    protected $guarded = [];

    protected $casts = [
        'base_price' => 'float',
        'price'      => 'float',
        'images'     => 'array',
        'is_public'  => 'boolean',
        'is_blocked' => 'boolean',
        'is_past'    => 'boolean',
    ];

    protected $dates = [
        'stock_date',
        'synced_at',
    ];

    protected static function boot()
    {
        parent::boot();

        // We need only published and unblocked products
        static::addGlobalScope('is_available', function (Builder $builder) {
            $builder->where('is_public', true)
                ->where('is_blocked', false);
        });
    }

    /**
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * GETTERS
     */

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getNameBrand(): ?string
    {
        return $this->name_brand;
    }

    /**
     * @return null|string
     */
    public function getNameModel(): ?string
    {
        return $this->name_model;
    }

    /**
     * @return null|string
     */
    public function getNameType(): ?string
    {
        return $this->name_type;
    }

    /**
     * @return null|string
     */
    public function getNameTypeGen(): ?string
    {
        return $this->name_type_gen;
    }

    /**
     * @return string
     */
    public function getNameTitle(): ?string
    {
        return $this->name_title;
    }

    /**
     * @return null|string
     */
    public function getNameGroup(): ?string
    {
        return $this->name_group;
    }

    /**
     * @return null|string
     */
    public function getNameColor(): ?string
    {
        return $this->name_color;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description
            ?: $this->getModel()->getDescription();
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        if (count($this->images) < 1) {
            return $this->getModelImages();
        }

        return $this->images;
    }

    /**
     * @return array
     */
    public function getModelImages(): array
    {
        if ($this->getModelId()) {
            return $this->getModel()->getImages();
        }

        return [];
    }

    /**
     * @return string
     */
    public function getPreview(): string
    {
        if (count($this->getImages()) > 0) {
            return $this->getImages()[0];
        }

        return '';
    }

    /**
     * @return float
     */
    public function getBasePrice(): float
    {
        return $this->base_price;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int|null
     */
    public function getModelId(): ?int
    {
        return $this->model_id;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brand_id;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @return int
     */
    public function getSubcategoryId(): int
    {
        return $this->subcategory_id;
    }

    /**
     * @return ProductModel|null
     */
    public function getModel(): ?ProductModel
    {
        return $this->model;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return Category|null
     */
    public function getSubcategory(): ?Category
    {
        return $this->subcategory;
    }

    /**
     * @return PropertyValue[]|mixed
     */
    public function getProperties()
    {
        return $this->property_values;
    }

    /**
     * Other model products
     *
     * @return mixed
     */
    public function getSiblings()
    {
        return Product::where('model_id', $this->getModelId())
            ->where('id', '!=', $this->getId())
            ->with('property_values')
            ->get();
    }

    /**
     * @param int $propertyId
     *
     * @return null|string
     */
    public function getProperty(int $propertyId)
    {
        /**
         * @var PropertyValue $propertyValue
         */
        $propertyValue = $this->getProperties()
            ->where('property_id', $propertyId)
            ->first();

        return $propertyValue && $propertyValue->getValue() ? $propertyValue->getValue() : null;
    }

    /**
     * @return null
     */
    public function getColor()
    {
        return $this->getProperty(Property::PROPERTY_PRODUCT_COLOR);
    }

    /**
     * @return null|string
     */
    public function getSize()
    {
        return $this->getProperty(Property::PROPERTY_PRODUCT_SIZE);
    }

    /**
     * @return int
     */
    public function getStockCount()
    {
        return $this->stock_count;
    }

    /**
     * RELATIONS
     */

    /**
     * Модель
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(ProductModel::class, 'model_id');
    }

    /**
     * Бренд
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    /**
     * Категория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Подкатегория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcategory()
    {
        return $this->belongsTo(Category::class, 'subcategory_id');
    }

    /**
     * Свойства товара
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function property_values()
    {
        return $this->hasMany(PropertyValue::class, 'object_id', 'id');
    }

    /**
     * BUSINESS LOGIC
     */

    /**
     * Check if product has any enabled promotion
     *
     * @return bool
     */
    public function hasPromotion(): bool
    {
        $currentPromotionsProductIds = Promotion::where('is_public', true)->pluck('products');
        $currentPromotionsProductIds = array_flatten($currentPromotionsProductIds);

        return in_array($this->getId(), $currentPromotionsProductIds);
    }

    /**
     * @return bool
     */
    public function hasDiscount(): bool
    {
        return $this->getBasePrice()
            && $this->getBasePrice() !== 0
            && $this->getBasePrice() !== $this->getPrice();
    }

    /**
     * @return bool
     */
    public function hasImages(): bool
    {
        return count($this->getImages()) > 0;
    }
}
