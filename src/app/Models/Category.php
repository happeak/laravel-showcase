<?php

namespace Happeak\Showcase\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 *
 * @property int $position
 * @property int $parent_id
 *
 * @property Carbon $synced_at
 *
 * @property Category $parent
 * @property Collection $children
 * @property Collection $products
 */
class Category extends Model
{

    protected $table   = 'shop_categories';

    protected $guarded = [];

    protected $dates = [
        'synced_at',
    ];

    /**
     * GETTERS
     */

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @return Category|mixed
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children()->orderBy('name')->get();
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return bool
     */
    public function hasProducts()
    {
        return count($this->products) > 0;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    /**
     * @return bool
     */
    public function isRoot()
    {
        return is_null($this->getParentId());
    }

    /**
     * @return bool
     */
    public function isChild()
    {
        return !$this->isRoot();
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    /**
     * SETTINGS
     */

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
