<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductModel
 * @package Happeak\Showcase\Models
 *
 * @property int $id
 * @property string $name
 * @property string $name_brand
 * @property string $name_model
 * @property string $name_type
 * @property string $name_type_gen
 * @property string $name_title
 * @property string $name_group
 * @property string $name_color
 * @property string $slug
 * @property string $description
 *
 * @property array $images
 *
 * @property int $brand_id
 * @property int $category_id
 * @property int $subcategory_id
 *
 * @property Brand $brand
 * @property Category $category
 * @property Category $subcategory
 * @property Collection $products
 * @property Collection $property_values
 */
class ProductModel extends Model
{

    /**
     * @var string
     */
    protected $table = 'shop_product_models';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'images' => 'array',
    ];

    protected $dates = [
        'synced_at',
    ];

    protected $perPage = 12;

    /**
     * GETTERS
     */

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getNameBrand(): ?string
    {
        return $this->name_brand;
    }

    /**
     * @return null|string
     */
    public function getNameModel(): ?string
    {
        return $this->name_model;
    }

    /**
     * @return null|string
     */
    public function getNameType(): ?string
    {
        return $this->name_type;
    }

    /**
     * @return null|string
     */
    public function getNameTypeGen(): ?string
    {
        return $this->name_type_gen;
    }

    /**
     * @return string
     */
    public function getNameTitle()
    {
        return $this->name_title;
    }

    /**
     * @return null|string
     */
    public function getNameGroup(): ?string
    {
        return $this->name_group;
    }

    /**
     * @return null|string
     */
    public function getNameColor(): ?string
    {
        return $this->name_color;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        if ($this->hasProducts()) {
            return $this->getFirstProduct()->getPrice();
        }

        return 0.0;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        if ($this->hasProperties()) {
            /**
             * @var PropertyValue $color
             */
            $color = $this->getProperties()
                ->where('property_id', Property::PROPERTY_MODEL_COLOR)
                ->first();

            if ($color) {
                return ucfirst(mb_strtolower($color->getValue()));
            }
        }

        return '';
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brand_id;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @return int
     */
    public function getSubcategoryId(): int
    {
        return $this->subcategory_id;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return Category|null
     */
    public function getSubcategory(): ?Category
    {
        return $this->subcategory;
    }

    /**
     * @return Collection
     */
    public function getProperties(): Collection
    {
        return $this->property_values;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return mixed
     */
    public function getFirstProduct(): ?Product
    {
        return $this->hasProducts()
            ? $this->getProducts()->first()
            : null;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed|string
     */
    public function getPreview()
    {
        if (count($images = $this->getImages()) > 0) {
            return $images[0];
        }

        return '';
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subcategory()
    {
        return $this->belongsTo(Category::class, 'subcategory_id', 'id');
    }

    /**
     * Model products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'model_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function property_values()
    {
        return $this->hasMany(PropertyValue::class, 'object_id', 'id');
    }

    /**
     * SCOPES
     */

    /**
     * @param Category $category
     * @param $query
     *
     * @return
     */
    public function scopeFilterByCategory($query, Category $category)
    {
        return $query->where('category_id', $category->id);
    }

    /**
     * @param Category $subcategory
     * @param $query
     *
     * @return
     */
    public function scopeFilterBySubCategory($query, Category $subcategory)
    {
        return $query->where('subcategory_id', $subcategory->id);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeFilterByHasAvailableProducts($query)
    {
        return $query->whereHas('products', function ($query) {
            $query->where('is_public', true)
                ->where('is_blocked', false);
        });
    }

    /**
     * @param $query
     * @param int $priceFrom
     * @param int $priceTo
     *
     * @return mixed
     */
    public function scopeFilterByPriceBetween($query, int $priceFrom, int $priceTo)
    {
        return $query->whereIn('id', Product::whereBetween('price', [
            $priceFrom,
            $priceTo,
        ])->pluck('model_id'));
    }

    /**
     * Filter by property value
     *
     * @param $query
     * @param int $propertyId
     * @param $value
     *
     * @return mixed
     */
    public function scopeFilterByPropertyValue($query, int $propertyId, $value)
    {
        return $query->whereHas('property_values', function ($query) use (&$propertyId, &$value) {
            $query->where('property_id', $propertyId)
                ->where('value', $value);
        });
    }

    /**
     * Filter by property values
     *
     * @param $query
     * @param int $propertyId
     * @param array $values
     *
     * @return mixed
     */
    public function scopeFilterByPropertyValueIn($query, int $propertyId, array $values)
    {
        return $query->whereHas('property_values', function ($query) use (&$propertyId, &$values) {
            $query->where('property_id', $propertyId)
                ->whereIn('value', $values);
        });
    }

    /**
     * Filter by available sizes
     *
     * @param $query
     * @param array $values
     *
     * @return mixed
     */
    public function scopeFilterBySize($query, array $values)
    {
        return $query->whereHas('products', function ($query) use (&$values) {
            return $query->whereHas('property_values', function ($query) use (&$values) {
                $query->whereIn('value', $values);
            });
        });
    }

    /**
     * SETTINGS
     */

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * BUSINESS LOGIC
     */

    /**
     * @return bool
     */
    public function hasProducts()
    {
        return count($this->getProducts()) > 0;
    }

    /**
     * @return bool
     */
    public function hasProperties()
    {
        return count($this->getProperties()) > 0;
    }

    /**
     * @return bool
     */
    public function hasDiscount(): bool
    {
        if ($this->hasProducts()) {
            foreach ($this->getProducts() as $product) {
                if ($product->hasDiscount()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasPromotion(): bool
    {
        if ($this->hasProducts()) {
            foreach ($this->getProducts() as $product) {
                /**
                 * @var Product $product
                 */
                if ($product->hasPromotion()) {
                    return true;
                }
            }
        }

        return false;
    }
}
