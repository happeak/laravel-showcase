<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Faq
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property int $position
 * @property FaqGroup $parent
 * @property Faq[]|Collection $questions
 */
class FaqGroup extends Model
{

    protected $table = 'shop_faq_groups';

    protected $casts = [];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @return FaqGroup|null
     */
    public function getParent(): ?FaqGroup
    {
        return $this->parent;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return is_null($this->parent_id);
    }

    /**
     * @return bool
     */
    public function isChild(): bool
    {
        return !$this->isRoot();
    }

    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(FaqGroup::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(Faq::class, 'group_id', 'id');
    }
}
