<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SiteMenuItem
 * @package App
 *
 * @property int $id
 * @property int $menu_id
 * @property int $parent_id
 * @property string $name
 * @property string $route
 * @property string $before_content
 * @property string $after_content
 * @property SiteMenu $menu
 * @property SiteMenuItem $parent
 * @property SiteMenuItem[] $children
 */
class SiteMenuItem extends Model
{

    protected $table = 'shop_menu_items';

    protected $casts = [];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getMenuId(): int
    {
        return $this->menu_id;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @return null|string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @return null|string
     */
    public function getBeforeContent(): ?string
    {
        return $this->before_content;
    }

    /**
     * @return null|string
     */
    public function getAfterContent(): ?string
    {
        return $this->after_content;
    }

    /**
     * @return SiteMenu
     */
    public function getMenu(): SiteMenu
    {
        return $this->menu;
    }

    /**
     * @return SiteMenuItem[]|Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return SiteMenuItem
     */
    public function getParent(): SiteMenuItem
    {
        return $this->parent;
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(SiteMenu::class, 'menu_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(SiteMenuItem::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(SiteMenuItem::class, 'parent_id', 'id');
    }
}
