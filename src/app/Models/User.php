<?php

namespace Happeak\Showcase\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $last_name
 * @property string $first_name
 * @property string $email
 * @property string $phone
 * @property int $bonus
 * @property string $city_name
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'shop_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name',
        'first_name',
        'email',
        'phone',
        'bonus',
        'city_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name
            ?: '';
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone
            ?: '';
    }

    /**
     * @return mixed
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    public function getBonus(): int
    {
        return $this->bonus;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }
}
