<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContentBlock
 * @package App
 *
 * @property int $id
 * @property string $block
 * @property string $content
 * @property string $content_en
 * @property string $content_ee
 * @property string $content_lv
 * @property bool $is_enabled
 */
class ContentBlock extends Model
{

    protected $table = 'shop_content_blocks';

    protected $casts = [
        'is_enabled' => 'boolean',
    ];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBlock(): string
    {
        return $this->block;
    }

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->is_enabled;
    }
}
