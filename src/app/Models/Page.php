<?php

namespace Happeak\Showcase\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * @package App
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property bool $is_enabled
 */
class Page extends Model
{

    protected $table = 'shop_pages';

    protected $casts = [
        'is_enabled' => 'boolean',
    ];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return null|string
     */
    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    /**
     * @return null|string
     */
    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    /**
     * @return null|string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->is_enabled;
    }
}
