<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FaqGroup
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $answer
 * @property string $form
 * @property string $button_label
 * @property int $group_id
 * @property int $position
 * @property boolean $is_public
 *
 * @property FaqGroup $group
 */
class Faq extends Model
{

    protected $table = 'shop_faqs';

    protected $casts = [
        'is_public' => 'boolean',
    ];

    protected $guarded = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * @return null|string
     */
    public function getForm(): ?string
    {
        return $this->form;
    }

    /**
     * @return null|string
     */
    public function getButtonLabel(): ?string
    {
        return $this->button_label;
    }

    /**
     * @return int|null
     */
    public function getGroupId(): ?int
    {
        return $this->group_id;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->is_public;
    }

    /**
     * @return FaqGroup|null
     */
    public function getGroup(): ?FaqGroup
    {
        return $this->group;
    }

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(FaqGroup::class, 'group_id', 'id');
    }
}
