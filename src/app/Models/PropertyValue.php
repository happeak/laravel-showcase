<?php

namespace Happeak\Showcase\Models;

use Happeak\Showcase\Traits\HasCompositePrimaryKey;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\PropertyValue
 *
 * @property Carbon|null   $created_at
 * @property Carbon|null   $updated_at
 * @property int           $property_id
 * @property int           $object_id
 * @property string        $destination
 * @property float|null    $value_num
 * @property int|null      $value_bool
 * @property string|null   $value_string
 * @property string|null   $value_text
 * @property-read Property $property
 * @property string|null   $value
 * @property string|null   $value_float
 * @property string|null   $name
 * @property string|null   $group
 * @property string|null   $unit
 * @property string|null   $position
 */
class PropertyValue extends Model
{

    use HasCompositePrimaryKey;

    /**
     * @var string
     */
    protected $table = 'shop_properties_values';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    public $primaryKey = [
        'object_id',
        'property_id',
        'destination',
    ];

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return null|string
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

    /**
     * @param        $query
     * @param string $destination
     *
     * @return mixed
     */
    public function scopeFilterByDestination($query, string $destination)
    {
        return $query->where('destination', $destination);
    }

    /**
     * @param     $query
     * @param int $objectId
     *
     * @return mixed
     */
    public function scopeFilterByObjectId($query, int $objectId)
    {
        return $query->where('object_id', $objectId);
    }

    /**
     * @param     $query
     * @param int $propertyId
     *
     * @return mixed
     */
    public function scopeFilterByPropertyId($query, int $propertyId)
    {
        return $query->where('property_id', $propertyId);
    }
}
