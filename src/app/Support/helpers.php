<?php

if (!function_exists('clean_phone')) {
    function clean_phone(string $phone): string
    {
        return preg_replace('/[^0-9.]+/', '', $phone);
    }
}

if (!function_exists('link_to_ecom')) {
    function link_to_ecom($url): string
    {
        if ($url[0] !== '/') {
            $url = '/' . $url;
        }

        return config('showcase.ecom_url') . $url;
    }
}

if (!function_exists('thumbnail')) {
    function thumbnail(string $path, string $format = 'happeak_product_175x200'): string
    {
        return $path
            ? config('showcase.thumbnail_service_url') . '/thumbnails/' . $format . '/' . $path
            : '';
    }
}

if (!function_exists('price')) {
    function price($price)
    {
        return number_format($price, 0, ',', ' ');
    }
}

if (!function_exists('link_to_category')) {
    function link_to_category(\Happeak\Showcase\Models\Category $category)
    {
        return url(sprintf('/catalog/%s', $category->getSlug()));
    }
}

if (!function_exists('link_to_subcategory')) {
    function link_to_subcategory(\Happeak\Showcase\Models\Category $subcategory)
    {
        return url(sprintf('/catalog/%s/%s', $subcategory->getParent()->getSlug(), $subcategory->getSlug()));
    }
}

if (!function_exists('link_to_product')) {
    function link_to_product(\Happeak\Showcase\Models\Product $product)
    {
        return url(sprintf('/catalog/%s/%s/%s', $product->getCategory()->getSlug(), $product->getSubcategory()->getSlug(), $product->getSlug()));
    }
}