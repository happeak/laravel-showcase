<?php

namespace Happeak\Showcase\Providers;

use Happeak\Showcase\Console\Commands\{SitemapGenerate,
    SyncBrands,
    SyncCategories,
    SyncContent,
    SyncProductModels,
    SyncProducts,
    SyncPromotions,
    SyncProperties};
use Illuminate\Support\ServiceProvider;

class ShowcaseServiceProvider extends ServiceProvider
{

    protected $defer = false;

    public function boot()
    {
        $this->loadDatabaseSchema();

        $this->loadConfigurations();

        $this->loadViews();

        $this->loadTranslationFiles();

        $this->loadConsoleCommands();

        $this->registerProviders();

        $this->loadAssets();

        $this->loadOauthProvider();
    }

    public function register()
    {
        if (!defined('SHOWCASE_PATH')) {
            define('SHOWCASE_PATH', realpath(__DIR__ . '/../../'));
        }
    }

    /**
     * Loading migrations, fixtures, etc
     */
    public function loadDatabaseSchema()
    {
        $this->loadMigrationsFrom(SHOWCASE_PATH . '/database/migrations');
    }

    /**
     * Load config files
     */
    public function loadConfigurations()
    {
        $this->publishes([
            SHOWCASE_PATH . '/config/showcase.php' => config_path('showcase.php'),
            SHOWCASE_PATH . '/config/services.php' => config_path('services.php'),
        ], 'config');

        $this->mergeConfigFrom(realpath(SHOWCASE_PATH . '/config/showcase.php'), 'showcase');
    }

    /**
     * Load views
     */
    public function loadViews()
    {
        $this->loadViewsFrom(resource_path('views/vendor/happeak/showcase'), 'showcase');
        $this->loadViewsFrom(realpath(SHOWCASE_PATH . '/resources/views'), 'showcase');

        $this->publishes([
            SHOWCASE_PATH . '/resources/views' => resource_path('views/vendor/happeak/showcase'),
        ], 'views');
    }

    /**
     * Load translations
     */
    public function loadTranslationFiles()
    {
        $this->loadTranslationsFrom(resource_path('lang/vendor/showcase'), 'showcase');
        $this->loadTranslationsFrom(realpath(SHOWCASE_PATH . '/resources/lang'), 'showcase');

        $this->publishes([
            SHOWCASE_PATH . '/resources/lang' => resource_path('lang/vendor/showcase'),
        ], 'lang');
    }

    /**
     * Load console commands
     */
    public function loadConsoleCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                SyncBrands::class,
                SyncCategories::class,
                SyncProductModels::class,
                SyncProducts::class,
                SyncPromotions::class,
                SyncProperties::class,
                SyncContent::class,
                SitemapGenerate::class,
            ]);
        }
    }

    /**
     * Load public assets
     */
    public function loadAssets()
    {
        $this->publishes([
            SHOWCASE_PATH . '/public' => public_path('vendor/showcase'),
        ], 'public');
    }

    /**
     * Register service providers
     */
    public function registerProviders()
    {
        foreach ($this->provides() as $provide) {
            $this->app->register($provide);
        }
    }

    /**
     * @return array
     */
    public function provides()
    {
        return [
            RouteServiceProvider::class,
            PechkinServiceProvider::class,
            ApiServiceProvider::class,
            FilterServiceProvider::class,
        ];
    }

    /**
     * Load Ecom Oauth Provider
     */
    public function loadOauthProvider()
    {
        // OAuth авторизация для Happeak E-commerce
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'ecom',
            function ($app) use ($socialite) {
                $config = $app['config']['services.ecom'];

                return $socialite->buildProvider(EcomOauthProvider::class, $config);
            }
        );
    }
}