<?php

namespace Happeak\Showcase\Providers;

use Happeak\Showcase\Models\{Category, Page, Product, ProductModel, Promotion};
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'Happeak\Showcase\Http\Controllers';

    public function boot()
    {
        $this->binding();

        parent::boot();
    }

    public function binding()
    {
        Route::bind('category', function ($value) {
            return Category::where('slug', $value)->whereNull('parent_id')->firstOrFail();
        });
        Route::bind('subcategory', function ($value) {
            return Category::where('slug', $value)->whereNotNull('parent_id')->firstOrFail();
        });
        Route::bind('product_model', function ($value) {
            return ProductModel::where('slug', $value)->firstOrFail();
        });
        Route::bind('product', function ($value) {
            return Product::where('slug', $value)->firstOrFail();
        });
        Route::bind('promotion', function ($value) {
            return Promotion::findOrFail($value);
        });
        Route::bind('page', function ($value) {
            return Page::where('is_enabled', true)->where('url', $value)->first();
        });
    }

    /**
     * Load application routes
     */
    public function map()
    {
        $this->loadRoutesFrom(SHOWCASE_PATH . '/routes/showcase.php');
    }
}