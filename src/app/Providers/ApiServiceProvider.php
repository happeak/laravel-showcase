<?php

namespace Happeak\Showcase\Providers;

use Happeak\ApiClient;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(ApiClient::class, function () {
            return new ApiClient(config('showcase.shop_id', 1), config('showcase.shop_secret', ''));
        });
    }
}