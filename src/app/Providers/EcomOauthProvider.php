<?php

namespace Happeak\Showcase\Providers;

use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class EcomOauthProvider extends AbstractProvider implements ProviderInterface
{

    /**
     * Get the authentication URL for the provider.
     *
     * @param  string $state
     *
     * @return string
     */
    protected function getAuthUrl($state)
    {
        return $this->buildAuthUrlFromBase(config('showcase.ecom_url') . DIRECTORY_SEPARATOR . 'oauth/authorize', $state);
    }

    /**
     * Get the token URL for the provider.
     *
     * @return string
     */
    protected function getTokenUrl()
    {
        return config('showcase.ecom_url') . DIRECTORY_SEPARATOR . 'oauth/token';
    }

    /**
     * Get the raw user for the given access token.
     *
     * @param  string $token
     *
     * @return array
     */
    protected function getUserByToken($token)
    {
        $response =
            $this->getHttpClient()->get(config('showcase.ecom_url') . DIRECTORY_SEPARATOR . 'oauth/api/user/profile', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
            ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Map the raw user array to a Socialite User instance.
     *
     * @param array $user
     *
     * @return User
     */
    protected function mapUserToObject(array $user)
    {
        return (new User)->setRaw($user)->map([
            'id'         => $user['id'],
            'email'      => $user['email'],
            'phone'      => $user['phone'],
            'last_name'  => $user['last_name'],
            'first_name' => $user['first_name'],
            'bonus'      => $user['bonus'],
            'city_name'  => $user['city_name'],
        ]);
    }
}
