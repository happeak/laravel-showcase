<?php

namespace Happeak\Showcase\Providers;

use Happeak\Showcase\Filters\Filters\FilterFactory;
use Illuminate\Support\ServiceProvider;

class FilterServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->singleton(FilterFactory::class);
    }
}