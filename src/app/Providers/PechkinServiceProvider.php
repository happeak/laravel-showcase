<?php

namespace Happeak\Showcase\Providers;

use Happeak\Showcase\Services\Pechkin;
use Illuminate\Support\ServiceProvider;

class PechkinServiceProvider extends ServiceProvider
{


    public function register()
    {
        $this->app->singleton(Pechkin::class);
    }
}