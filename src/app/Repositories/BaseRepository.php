<?php

namespace Happeak\Showcase\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseRepository
 * @package App\Repositories
 */
abstract class BaseRepository
{

    /**
     * @var Builder $builder
     */
    protected $builder;

    /**
     * @var Model $model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->builder = ($this->model)::query();
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getModelInstance()
    {
        return new $this->model;
    }

    /**
     * @param $model
     *
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get()
    {
        return $this->builder->get();
    }

    /**
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate(int $perPage)
    {
        return $this->builder->paginate($perPage);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Model[]
     */
    public function all()
    {
        return $this->builder->all();
    }

    /**
     * @param int $id
     *
     * @return BaseRepository
     */
    public function filterByKey(int $id): BaseRepository
    {
        $this->builder->where('id', $id);

        return $this;
    }

    /**
     * @param array $keys
     *
     * @return BaseRepository
     */
    public function filterByKeys(array $keys): BaseRepository
    {
        $this->builder->whereIn('id', $keys);

        return $this;
    }

    /**
     * @param array $keys
     *
     * @return $this
     */
    public function filterExcludeKeys(array $keys)
    {
        $this->builder->whereNotIn('id', $keys);

        return $this;
    }


    /**
     * @param array $relations
     *
     * @return BaseRepository
     */
    public function withRelations(array $relations): BaseRepository
    {
        $this->builder->with($relations);

        return $this;
    }
}