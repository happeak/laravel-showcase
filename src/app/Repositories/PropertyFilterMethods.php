<?php

namespace Happeak\Showcase\Repositories;

trait PropertyFilterMethods
{

    /**
     * @param int $propertyId
     * @param $value
     *
     * @return ProductModelRepository
     */
    public function filterByPropertyValue(int $propertyId, $value)
    {
        $this->builder->whereHas('property_values', function ($query) use (&$propertyId, &$value) {
            $query->where('property_id', $propertyId)
                ->where('value', $value);
        });

        return $this;
    }

    /**
     * @param int $propertyId
     * @param array $values
     *
     * @return ProductModelRepository
     */
    public function filterByPropertyValueIn(int $propertyId, array $values)
    {
        $this->builder->whereHas('property_values', function ($query) use (&$propertyId, &$values) {
            $query->where('property_id', $propertyId)
                ->whereIn('value', $values);
        });

        return $this;
    }
}