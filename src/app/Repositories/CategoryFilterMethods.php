<?php

namespace Happeak\Showcase\Repositories;

use Happeak\Showcase\Models\Category;

trait CategoryFilterMethods
{

    /**
     * @param Category $category
     *
     * @return $this
     */
    public function filterByCategory(Category $category)
    {
        $this->builder->where('category_id', $category->getId());

        return $this;
    }

    /**
     * @param Category $subcategory
     *
     * @return ProductModelRepository
     */
    public function filterBySubcategory(Category $subcategory)
    {
        $this->builder->where('subcategory_id', $subcategory->getId());

        return $this;
    }

    /**
     * @param int $categoryId
     *
     * @return ProductModelRepository
     */
    public function filterByCategoryId(int $categoryId)
    {
        $this->builder->where('category_id', $categoryId);

        return $this;
    }

    /**
     * @param int $subcategoryId
     *
     * @return ProductModelRepository
     */
    public function filterBySubcategoryId(int $subcategoryId)
    {
        $this->builder->where('subcategory_id', $subcategoryId);

        return $this;
    }
}