<?php

namespace Happeak\Showcase\Repositories;

use Happeak\Showcase\Models\{Product, ProductModel, Property};

class ProductModelRepository extends BaseRepository
{

    use PropertyFilterMethods, CategoryFilterMethods;

    public function __construct()
    {
        $this->setModel(ProductModel::class);
        parent::__construct();
    }

    /**
     * @param string $term
     *
     * @return $this
     */
    public function filterByTerm(string $term)
    {
        $term = strip_tags($term);

        $this->builder->whereRaw('MATCH(name, description) AGAINST(\'' . $term . '\')');

        return $this;
    }

    /**
     * @param int $price
     *
     * @return ProductModelRepository
     */
    public function filterByPriceFrom(int $price = 0)
    {
        $this->builder->whereIn('id', Product::where('price', '>=', $price)->pluck('model_id'));

        return $this;
    }

    /**
     * @param int $price
     *
     * @return ProductModelRepository
     */
    public function filterByPriceTo(int $price = 1000000)
    {
        $this->builder->whereIn('id', Product::where('price', '<=', $price)->pluck('model_id'));

        return $this;
    }

    /**
     * @param int $priceFrom
     * @param int $priceTo
     *
     * @return $this
     */
    public function filterByPriceBetween(int $priceFrom = 0, int $priceTo = 1000000)
    {
        $this->builder->whereIn('id', Product::whereBetween('price', [
            $priceFrom,
            $priceTo,
        ])->pluck('model_id'));

        return $this;
    }

    /**
     * @param array $colors
     *
     * @return $this
     */
    public function filterByColors(array $colors)
    {
        $this->filterByPropertyValueIn(Property::PROPERTY_MODEL_COLOR, $colors);

        return $this;
    }

    /**
     * @param array $sizes
     *
     * @return $this
     */
    public function filterBySizes(array $sizes)
    {
        $this->builder->whereHas('products', function ($query) use (&$sizes) {
            $query->whereHas('property_values', function ($query) use (&$sizes) {
                $query->where('property_id', Property::PROPERTY_PRODUCT_SIZE)
                    ->whereIn('value', $sizes);
            });
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByHasProducts()
    {
        $this->builder->whereHas('products');

        return $this;
    }
}