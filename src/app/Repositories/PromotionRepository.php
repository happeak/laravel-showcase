<?php

namespace Happeak\Showcase\Repositories;

use Happeak\Showcase\Models\Promotion;

class PromotionRepository extends BaseRepository
{

    public function __construct()
    {
        $this->setModel(Promotion::class);
        parent::__construct();
    }

    /**
     * @return PromotionRepository
     */
    public function filterByPublic()
    {
        $this->builder->where('is_public', true);

        return $this;
    }

    /**
     * @return PromotionRepository
     */
    public function filterByActive()
    {
        $this->builder
            ->where('expire_at', '>', now())
            ->where('start_at', '<', now())
            ->orWhereNull('start_at');

        return $this;
    }
}