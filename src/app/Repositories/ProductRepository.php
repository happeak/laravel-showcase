<?php

namespace Happeak\Showcase\Repositories;

use Happeak\Showcase\Models\Product;
use Happeak\Showcase\Models\Property;

class ProductRepository extends BaseRepository
{

    use PropertyFilterMethods, CategoryFilterMethods;

    public function __construct()
    {
        $this->setModel(Product::class);
        parent::__construct();
    }

    /**
     * @return $this
     */
    public function filterByHasPrice()
    {
        $this->builder->where('price', '>', 0);

        return $this;
    }

    /**
     * @param int $price
     *
     * @return ProductRepository
     */
    public function filterByPriceFrom(int $price = 0)
    {
        $this->builder->where('price', '>=', $price);

        return $this;
    }

    /**
     * @param int $price
     *
     * @return ProductRepository
     */
    public function filterByPriceTo(int $price = 1000000)
    {
        $this->builder->where('price', '<=', $price);

        return $this;
    }

    /**
     * @param int $priceFrom
     * @param int $priceTo
     *
     * @return $this
     */
    public function filterByPriceBetween(int $priceFrom = 0, int $priceTo = 1000000)
    {
        $this->builder->whereBetween('price', [
            $priceFrom,
            $priceTo,
        ]);

        return $this;
    }

    /**
     * @param array $sizes
     *
     * @return $this
     */
    public function filterBySizes(array $sizes)
    {
        $this->filterByPropertyValueIn(Property::PROPERTY_PRODUCT_SIZE, $sizes);

        return $this;
    }

    /**
     * @param array $colors
     *
     * @return $this
     */
    public function filterByColors(array $colors)
    {
        $this->filterByPropertyValueIn(Property::PROPERTY_PRODUCT_COLOR, $colors);

        return $this;
    }

    /**
     * @param array $brandIds
     *
     * @return $this
     */
    public function filterByBrands(array $brandIds)
    {
        $this->builder->whereIn('brand_id', $brandIds);

        return $this;
    }

    /**
     * @return $this
     */
    public function filterByStockAvailable()
    {
        $this->builder->where('stock_count', '>', 0);

        return $this;
    }
}