<?php

namespace Happeak\Showcase\Services;

use Happeak\Showcase\Models\{Brand,
    Category,
    ContentBlock,
    Page,
    Product,
    ProductModel,
    Promotion,
    Property,
    PropertyValue,
    Banner,
    SiteMenu,
    SiteMenuItem};
use Carbon\Carbon;

class Synchronizer
{

    /**
     * @param array $brands
     *
     * @return bool
     */
    public function brands(array $brands = []): bool
    {
        if (count($brands) > 0) {
            $brands =
                array_key_exists('brands', $brands)
                    ? $brands['brands']
                    : $brands;

            foreach ($brands as $id => $brand) {
                Brand::updateOrCreate([
                    'id' => $id,
                ], [
                    'name' => $brand['name'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * @param array $categories
     *
     * @return bool
     */
    public function categories(array $categories): bool
    {
        if (count($categories) > 0) {
            $categories = array_key_exists('categories', $categories)
                ? $categories['categories']
                : $categories;

            foreach ($categories as $id => $category) {
                Category::updateOrCreate([
                    'id' => $id,
                ], [
                    'name'      => $category['name'],
                    'parent_id' => $category['parent_id'],
                    'slug'      => $category['slug'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация данных моделей
     *
     * @param array $models
     *
     * @return bool
     */
    public function models(array $models = []): bool
    {
        if (count($models) > 0) {

            $models =
                array_key_exists('models', $models)
                    ? $models['models']
                    : $models;

            foreach ($models as $id => $model) {
                ProductModel::updateOrCreate([
                    'id' => $id,
                ], [
                    'brand_id'       => $model['brand_id'],
                    'category_id'    => ($category = Category::find($model['category_id']))
                        ? $category->id
                        : null,
                    'subcategory_id' => ($model['subcategory_id'] && $subcategory =
                            Category::find($model['subcategory_id']))
                        ? $subcategory->id
                        : null,
                    'name'           => $model['name'],
                    'description'    => $model['description'],
                    'name_type'      => $model['name_type'],
                    'name_type_gen'  => $model['name_type_gen'],
                    'name_brand'     => $model['name_brand'],
                    'name_title'     => $model['name_title'],
                    'name_group'     => $model['name_group'],
                    'name_model'     => $model['name_model'],
                    'synced_at'      => Carbon::now(),
                    'images'         => array_map(function ($image) {
                        return str_replace('https://www.happeak.ru/images', '', $image);
                    }, $model['images']),
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация данных товаров
     *
     * @param array $products
     *
     * @return bool
     */
    public function products(array $products = [])
    {
        if (count($products) > 0) {

            $products =
                array_key_exists('products', $products)
                    ? $products['products']
                    : $products;

            foreach ($products as $id => $product) {
                try {
                    Product::updateOrCreate([
                        'id' => $id,
                    ], [
                        'name'           => $product['name'],
                        'name_type'      => $product['name_type'],
                        'name_type_gen'  => $product['name_type_gen'],
                        'name_brand'     => $product['name_brand'],
                        'name_model'     => $product['name_model'],
                        'name_title'     => $product['name_title'],
                        'name_color'     => $product['name_color'],
                        'name_group'     => $product['name_group'],
                        'description'    => $product['description'],
                        'slug'           => $product['slug'],
                        'model_id'       => ($product['model_id'] && $model =
                                ProductModel::find($product['model_id']))
                            ? $model->id
                            : null,
                        'brand_id'       => Brand::find($product['brand_id'])->id,
                        'category_id'    => Category::find($product['category_id'])->id,
                        'subcategory_id' => ($product['subcategory_id'] && $subcategory =
                                Category::find($product['subcategory_id']))
                            ? $subcategory->id
                            : null,
                        'images'         => array_map(function ($image) {
                            return str_replace('https://www.happeak.ru/images', '', $image);
                        }, $product['images']),
                        'stock_date'     => $product['stock_date'],
                        'stock_count'    => $product['stock_count'],
                        'price'          => $product['price'],
                        'base_price'     => $product['base_price'],
                        'is_public'      => $product['is_public'],
                        'is_blocked'     => $product['is_blocked'],
                    ]);
                } catch (\Exception $exception) {
                    continue;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация справочника характеристик
     *
     * @param array $options
     *
     * @return bool
     */
    public function options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $option) {
                Property::updateOrCreate([
                    'id' => $option['id'],
                ], [
                    'name'          => $option['name'],
                    'group'         => $option['group'],
                    'unit'          => $option['unit'],
                    'destination'   => $option['destination'],
                    'default_value' => $option['default_value'],
                    'values'        => $option['values'],
                    'category_id'   => ($category = Category::find($option['category_id']))
                        ? $category->id
                        : null,
                    'is_filter'     => $option['is_filter'],
                    'position'      => $option['position'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация характеристик моделей товаров
     *
     * @param array $options
     *
     * @return bool
     */
    public function model_options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $optionGroup) {
                foreach ($optionGroup as $option) {
                    $property = Property::find($option['option_id']);
                    $model = ProductModel::find($option['model_id']);

                    PropertyValue::updateOrCreate([
                        'object_id'   => $model->id,
                        'property_id' => $property->id,
                        'destination' => 'model',
                    ], [
                        'value'        => $option['value'],
                        'value_num'    => $option['value_int'],
                        'value_float'  => $option['value_float'],
                        'value_text'   => $option['value_text'],
                        'value_string' => $option['value_str'],
                        'name'         => $option['name'],
                        'group'        => $option['group'],
                        'unit'         => $option['unit'],
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация характеристик товаров
     *
     * @param array $options
     *
     * @return bool
     */
    public function product_options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $optionGroup) {
                foreach ($optionGroup as $option) {
                    $property = Property::find($option['option_id']);
                    $product = Product::find($option['product_id']);

                    PropertyValue::updateOrCreate([
                        'object_id'   => $product->id,
                        'property_id' => $property->id,
                        'destination' => 'product',
                    ], [
                        'value'        => $option['value'],
                        'value_num'    => $option['value_int'],
                        'value_float'  => $option['value_float'],
                        'value_text'   => $option['value_text'],
                        'value_string' => $option['value_str'],
                        'name'         => $option['name'],
                        'group'        => $option['group'],
                        'unit'         => $option['unit'],
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация акций
     *
     * @param array $promotions
     *
     * @return bool
     */
    public function promotions(array $promotions = [])
    {
        if (count($promotions) > 0) {

            foreach ($promotions['promotions'] as $promotion) {
                Promotion::updateOrCreate([
                    'id' => $promotion['id'],
                ], [
                    'name'           => $promotion['name'],
                    'title'          => $promotion['title'],
                    'description'    => $promotion['description'],
                    'discount'       => $promotion['discount'],
                    'is_public'      => $promotion['is_public'],
                    'start_at'       => $promotion['start_at'],
                    'expire_at'      => $promotion['expire_at'],
                    'products'       => $promotion['products'],
                    'only_available' => $promotion['only_available'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * @param array $banners
     *
     * @return bool
     */
    public function banners(array $banners)
    {
        if (!empty($banners)) {

            foreach ($banners['banners'] as $banner) {
                Banner::updateOrCreate([
                    'id' => $banner['id'],
                ], [
                    'url'          => $banner['url'],
                    'title'        => $banner['title'],
                    'image'        => $banner['image'],
                    'image_mobile' => $banner['image_mobile'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * @param array $contentBlocks
     *
     * @return bool
     */
    public function content_blocks(array $contentBlocks)
    {
        if (!empty($contentBlocks)) {

            foreach ($contentBlocks['blocks'] as $contentBlock) {
                ContentBlock::updateOrCreate([
                    'id' => $contentBlock['id'],
                ], [
                    'block'      => $contentBlock['block'],
                    'content'    => $contentBlock['content'],
                    'is_enabled' => $contentBlock['is_enabled'],
                ]);
            }

            return true;

        }

        return false;
    }

    /**
     * @param array $pages
     *
     * @return bool
     */
    public function pages(array $pages)
    {
        if (!empty($pages)) {

            foreach ($pages['pages'] as $page) {
                Page::updateOrCreate([
                    'id' => $page['id'],
                ], [
                    'title'            => $page['title'],
                    'url'              => $page['url'],
                    'content'          => $page['content'],
                    'meta_title'       => $page['meta_title'],
                    'meta_description' => $page['meta_description'],
                    'meta_keywords'    => $page['meta_keywords'],
                    'is_enabled'       => $page['is_enabled'],
                ]);
            }

            return true;

        }

        return false;
    }

    /**
     * @param array $menus
     *
     * @return bool
     */
    public function menus(array $menus)
    {
        if (!empty($menus)) {

            foreach ($menus['menus'] as $menu) {
                SiteMenu::updateOrCreate([
                    'id' => $menu['id'],
                ], [
                    'name' => $menu['name'],
                ]);

                if (array_key_exists('items', $menu) && !empty($menu['items'])) {
                    foreach ($menu['items'] as $item) {
                        SiteMenuItem::updateOrCreate([
                            'id' => $item['id'],
                        ], [
                            'menu_id'        => $item['menu_id'],
                            'parent_id'      => $item['parent_id'],
                            'name'           => $item['name'],
                            'route'          => $item['route'],
                            'before_content' => $item['before_content'],
                            'after_content'  => $item['after_content'],
                        ]);
                    }
                }
            }

        }

        return false;
    }
}