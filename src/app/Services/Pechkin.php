<?php

namespace Happeak\Showcase\Services;

use Happeak\Showcase\Models\User;
use HappeakApi\Pechkin\Api;
use HappeakApi\Pechkin\Endpoint;

class Pechkin
{

    protected $api = null;


    /**
     * Send notification
     *
     * @param string $id
     * @param User $user
     * @param array $params
     * @param array $files
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function notifySend(string $id, User $user, array $params = [], array $files = []): array
    {
        $this->syncUser($user);

        $request = $this->getApi()
            ->request(Endpoint::notifySend($id))
            ->param($params)
            ->param('email', $user->getEmail());

        foreach ($files as $name => $path) {
            $request->attachment($path, $name);
        }

        return $request->call();
    }


    /**
     * Synchronize user
     *
     * @param User $user
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function syncUser(User $user): array
    {
        $request = $this->getApi()->request(Endpoint::contactSync($user->getEmail()));

        return $request->call();
    }


    /**
     * Returns Pechkin Api instance
     *
     * @return Api|null
     */
    protected function getApi(): Api
    {
        if (null === $this->api) {
            $this->api = new Api([
                'project-id'  => config('showcase.pechkin_id'),
                'project-key' => config('showcase.pechkin_secret'),
            ]);
        }

        return $this->api;
    }
}