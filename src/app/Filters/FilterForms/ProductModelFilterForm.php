<?php

namespace Happeak\Showcase\Filters\FilterForms;

use Happeak\Showcase\Models\Brand;
use Happeak\Showcase\Models\Category;
use Happeak\Showcase\Models\Product;
use Happeak\Showcase\Models\ProductModel;

class ProductModelFilterForm extends BaseFilterForm
{

    /**
     * @return $this
     */
    public function addBrandSection()
    {
        $brandIds = $this->getCollection()->pluck('brand_id')->unique();
        $brands = Brand::whereIn('id', $brandIds)->orderBy('name')->get()->pluck('name', 'id')->toArray();

        $this->addSection('brands', 'checkbox', $brands);

        return $this;
    }

    /**
     * @return $this
     */
    public function addSubcategorySection()
    {
        $subcategoryIds = $this->getCollection()->pluck('subcategory_id')->unique();
        $subcategories =
            Category::whereNotNull('parent_id')->whereIn('id', $subcategoryIds)->orderBy('name')->get();

        $this->addSection('category', 'subcategory', [
            'subcategories' => $subcategories,
        ]);

        return $this;
    }

    /**
     * @return $this
     */
    public function addColorSection()
    {
        $colors = $this->getCollection()
            ->map(function ($productModel) {
                /**
                 * @var ProductModel $productModel
                 */
                return $productModel->getColor();
            })
            ->unique()
            ->filter(function ($color) {
                return !is_null($color);
            })
            ->sortBy(function ($color) {
                return $color;
            })
            ->toArray();

        // array_combine нужен для того, чтобы ключи массива были наименованиями цветов, т.к. ключи массива подставляются
        // в value input с типом checkbox и фильтрация производится по названиям цветов
        $this->addSection('colors', 'checkbox', array_combine($colors, $colors));

        return $this;
    }

    /**
     * @return $this
     */
    public function addSizeSection()
    {
        $products = Product::whereIn('model_id', $this->getCollection()->pluck('id'))->with(['property_values'])->get();

        $sizes = $products
            ->map(function ($product) {
                /**
                 * @var ProductModel $product
                 */
                return $product->getSize();
            })
            ->unique()
            ->filter(function ($size) {
                return !is_null($size);
            })
            ->sortBy(function ($size) {
                return $size;
            })
            ->toArray();

        $this->addSection('sizes', 'checkbox', array_combine($sizes, $sizes));

        return $this;
    }

    /**
     * @return $this
     */
    public function addPriceSection()
    {
        //        $priceFrom = $this->getCollection()->pluck('price')->unique()->min();
        //        $priceTo = $this->getCollection()->pluck('price')->unique()->max();
        $this->addSection('price', 'price', [
            'price_from' => 0,
            'price_to'   => 100000,
        ]);

        return $this;
    }
}