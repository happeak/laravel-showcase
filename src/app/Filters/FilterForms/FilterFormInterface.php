<?php

namespace Happeak\Showcase\Filters\FilterForms;


interface FilterFormInterface
{

    /**
     * @param string $name
     * @param string $type
     * @param array $data
     *
     * @return mixed
     */
    public function addSection(string $name, string $type, array $data = []);

    /**
     * @param string $view
     *
     * @return mixed
     */
    public function setView(string $view);

    /**
     * @return mixed
     */
    public function render();
}