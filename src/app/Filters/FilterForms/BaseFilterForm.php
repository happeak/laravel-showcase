<?php

namespace Happeak\Showcase\Filters\FilterForms;

use Happeak\Showcase\Filters\FilterForms\Sections\{CheckboxSection, InputSection, PriceSection, SubcategorySection};
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BaseFilterForm
 * @package Happeak\Showcase\Filters\FilterForms
 */
abstract class BaseFilterForm implements FilterFormInterface
{

    /**
     * @var Collection $collection
     */
    protected $collection;

    /**
     * @var array $sections
     */
    protected $sections = [];

    /**
     * @var string $view
     */
    protected $view = 'showcase::filters.default';

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @return Collection
     */
    public function getCollection(): Collection
    {
        return $this->collection;
    }

    /**
     * @param $collection
     *
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * @param string $view
     *
     * @return $this
     */
    public function setView(string $view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->getSections());
    }

    /**
     * @return bool
     */
    public function isNotEmpty()
    {
        return !$this->isEmpty();
    }

    /**
     * @param string $name
     * @param string $type
     * @param array $data
     *
     * @return void
     */
    public function addSection(string $name, string $type, array $data = [])
    {
        switch ($type) {
            case 'input':
                $this->sections[] = new InputSection($name, $data);
                break;
            case 'checkbox':
                $this->sections[] = new CheckboxSection($name, $data);
                break;
            case 'price':
                $this->sections[] = new PriceSection($name, $data);
                break;
            case 'subcategory':
                $this->sections[] = new SubcategorySection($name, $data);
                break;
            default:
                throw new \InvalidArgumentException('Filter section with type ' . $type . ' does not exist');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return view($this->getView(), [
            'filterForm' => $this,
        ]);
    }
}