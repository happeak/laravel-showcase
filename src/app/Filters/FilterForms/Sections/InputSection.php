<?php

namespace Happeak\Showcase\Filters\FilterForms\Sections;

class InputSection extends BaseFilterFormSection
{

    protected $view = 'showcase::filters.sections.input';
}