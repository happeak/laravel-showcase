<?php

namespace Happeak\Showcase\Filters\FilterForms\Sections;

class PriceSection extends BaseFilterFormSection
{

    protected $view = 'showcase::filters.sections.price';
}