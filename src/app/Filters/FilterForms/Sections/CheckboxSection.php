<?php

namespace Happeak\Showcase\Filters\FilterForms\Sections;

class CheckboxSection extends BaseFilterFormSection
{

    protected $view = 'showcase::filters.sections.checkbox';
}