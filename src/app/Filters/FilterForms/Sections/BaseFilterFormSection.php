<?php

namespace Happeak\Showcase\Filters\FilterForms\Sections;

abstract class BaseFilterFormSection
{

    /**
     * Required for translations
     *
     * @var string $name
     */
    protected $name;

    /**
     * @var array $data
     */
    protected $data = [];

    /**
     * @var string $view
     */
    protected $view;

    /**
     * BaseFilterFormSection constructor.
     *
     * @param string $name
     * @param array $data
     */
    public function __construct(string $name, array $data = [])
    {
        $this->name = $name;
        $this->data = $data;
    }

    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitle(): ?string
    {
        return trans('showcase::filters.sections.' . $this->getName() . '.title');
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function getDefault(string $key)
    {
        return array_key_exists($key, $this->getData())
            ? $this->getData()[$key]
            : null;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function render()
    {
        return view($this->getView(), [
            'section'  => $this,
        ]);
    }
}