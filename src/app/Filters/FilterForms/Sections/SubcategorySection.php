<?php

namespace Happeak\Showcase\Filters\FilterForms\Sections;

class SubcategorySection extends BaseFilterFormSection
{

    protected $view = 'showcase::filters.sections.subcategory';
}