<?php

namespace Happeak\Showcase\Filters;

use Happeak\Showcase\Filters\FilterForms\FilterFormInterface;
use Happeak\Showcase\Filters\FilterForms\ProductModelFilterForm;
use Happeak\Showcase\Filters\FilterForms\ProductFilterForm;

class FilterFormBuilder
{

    /**
     * @param string $type
     *
     * @return mixed
     */
    public function createForm(string $type = 'product'): FilterFormInterface
    {
        switch ($type) {
            case 'model':
                return new ProductModelFilterForm();
            case 'product':
                return new ProductFilterForm();
            default:
                return new ProductFilterForm();
        }
    }
}