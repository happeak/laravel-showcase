<?php

namespace Happeak\Showcase\Filters\Filters;

class FilterFactory
{

    /**
     * @param string $type
     *
     * @return ProductFilter|ProductModelFilter
     */
    public function createFilter(string $type)
    {
        switch ($type) {
            case 'model':
                return new ProductModelFilter;
            case 'product':
                return new ProductFilter;
            default:
                throw new \InvalidArgumentException('Filter with type ' . $type . ' not found');
        }
    }
}