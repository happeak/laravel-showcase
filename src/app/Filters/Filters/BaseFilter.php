<?php

namespace Happeak\Showcase\Filters\Filters;

use Happeak\Showcase\Repositories\ProductModelRepository;

class BaseFilter
{

    private $availableProperties = [
        'price_from'     => 0,
        'price_to'       => 1000000,
        'category_id'    => null,
        'subcategory_id' => null,
        'term'           => null,
        'sizes'          => [],
        'colors'         => [],
        'brands'         => [],
    ];

    private $parameters = [];

    private $repository;

    protected $query;

    /**
     * FilterHandler constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return array
     */
    public function getAvailableProperties(): array
    {
        return $this->availableProperties;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param $query
     *
     * @return $this
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return ProductModelRepository
     */
    public function getRepository()
    {
        return new $this->repository;
    }

    /**
     * @param $repository
     *
     * @return BaseFilter
     */
    public function setRepository($repository)
    {
        if (!class_exists($repository)) {
            throw new \InvalidArgumentException($repository . ' class does not exist');
        }

        $this->repository = new $repository;

        return $this;
    }

    /**
     * @param string $parameterKey
     * @param $parameterValue
     *
     * @return $this
     */
    public function setParameter(string $parameterKey, $parameterValue)
    {
        $this->parameters[$parameterKey] = $parameterValue;

        return $this;
    }

    /**
     * @param array $parameters
     *
     * @return $this
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = array_merge($this->parameters, $parameters);

        return $this;
    }

    /**
     * @param string $parameter
     *
     * @return bool
     */
    public function checkIfParameterIsPermitted(string $parameter)
    {
        return in_array($parameter, array_keys($this->getAvailableProperties()));
    }

    /**
     * @param string $parameter
     *
     * @return bool
     */
    public function checkIfParameterIsNotPermitted(string $parameter)
    {
        return !$this->checkIfParameterIsPermitted($parameter);
    }

    /**
     * @param string $parameter
     *
     * @return string
     */
    public function getFilterMethodForParameter(string $parameter)
    {
        return 'filterBy' . ucfirst(camel_case($parameter));
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function checkIfValueIsSet($value)
    {
        return is_scalar($value) && $value || is_array($value) && !empty($value);
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function checkIfValueIsNotSet($value)
    {
        return !$this->checkIfValueIsSet($value);
    }

    /**
     * @param string $method
     * @param null $repository
     *
     * @return bool
     */
    public function checkIfMethodExists(string $method, $repository = null)
    {
        if (!$repository) {
            $repository = $this->getRepository();
        }

        return method_exists($method, get_class($repository));
    }

    /**
     * @param string $method
     * @param null $repository
     *
     * @return bool
     */
    public function checkIfMethodNotExists(string $method, $repository = null)
    {
        return !$this->checkIfMethodExists($method, $repository);
    }

    /**
     * @return void
     */
    public function filter(): void
    {
        $this->query = $this->getRepository();
        foreach ($this->getParameters() as $param => $value) {

            if ($this->checkIfParameterIsNotPermitted($param) || $this->checkIfValueIsNotSet($value))
                continue;

            $filterMethod = $this->getFilterMethodForParameter($param);

            $this->query = $this->query->$filterMethod($value);
        }
    }

    /**
     * @param int|null $perPage
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function execute(?int $perPage = null)
    {
        $this->filter();

        if ($perPage) {
            return $this->query->paginate($perPage);
        }

        return $this->query->get();
    }

    /**
     * @param array $relations
     * @param int|null $perPage
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function executeWithRelations(array $relations = [], ?int $perPage = null)
    {
        $this->filter();

        $this->query = $this->query->withRelations($relations);

        if ($perPage) {
            return $this->query->paginate($perPage);
        }

        return $this->query->get();
    }
}