<?php

namespace Happeak\Showcase\Filters\Filters;

use Happeak\Showcase\Repositories\ProductRepository;

class ProductFilter extends BaseFilter
{

    public function __construct()
    {
        $this->setRepository(ProductRepository::class);
        parent::__construct();
    }
}