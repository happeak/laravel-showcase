<?php

namespace Happeak\Showcase\Filters\Filters;

use Happeak\Showcase\Repositories\ProductModelRepository;

class ProductModelFilter extends BaseFilter
{

    /**
     * ProductModelFilter constructor.
     */
    public function __construct()
    {
        $this->setRepository(ProductModelRepository::class);
        parent::__construct();
    }
}