<?php

namespace Happeak\Showcase\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SubscribeRequest
 * @package Happeak\Showcase\Http\Requests
 *
 * @property string $email
 */
class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }
}
