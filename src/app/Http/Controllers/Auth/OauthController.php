<?php

namespace Happeak\Showcase\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Happeak\Showcase\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class OauthController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @param $provider
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @param $provider
     *
     * @return Response
     * @throws \Exception
     */
    public function handleProviderCallback(string $provider)
    {
        $socialiteUser = Socialite::driver($provider)->stateless()->user();

        switch ($provider) {
            case 'ecom':
                $user = User::updateOrCreate([
                    'email' => $socialiteUser['email'],
                ], [
                    'id'         => $socialiteUser['id'],
                    'last_name'  => $socialiteUser['last_name'],
                    'first_name' => $socialiteUser['first_name'],
                    'phone'      => $socialiteUser['phone'],
                    'city_name'  => $socialiteUser['city_name'],
                    'bonus'      => $socialiteUser['bonus'],
                ]);

                Auth::login($user, true);

                return redirect()->intended(route('homepage'));
            default:
                abort(404);
                break;
        }
    }
}