<?php

namespace Happeak\Showcase\Http\Controllers;

use App\Http\Controllers\Controller;
use Happeak\ApiClient;
use Happeak\Showcase\Http\Requests\SubscribeRequest;

class SubscribeController extends Controller
{

    /**
     * Подписка на новости
     *
     * @param SubscribeRequest $request
     * @param ApiClient $happeak
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(SubscribeRequest $request, ApiClient $happeak)
    {
        if (!$request->ajax()) {
            abort(404);
        }
        $happeak->subscribe->subscribe($request->email, 27);

        return response()->json([
            'error'   => false,
            'message' => trans('subscribe.succcess'),
        ]);
    }
}