<?php

namespace Happeak\Showcase\Http\Controllers;

use Happeak\Showcase\Models\Page;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('showcase::content.index');
    }

    /**
     * @param Page $page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Page $page)
    {
        return view('showcase::content.show', [
            'page' => $page,
        ]);
    }
}