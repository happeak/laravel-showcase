<?php

namespace Happeak\Showcase\Http\Controllers;

use App\Http\Controllers\Controller;

class PromocodeController extends Controller
{

    /**
     * Redirect to Happeak E-commerce to set promocode
     *
     * @param string $promocode
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function setPromocode(string $promocode)
    {
        return redirect(link_to_ecom('store/' . config('showcase.ecom_slug') . '/cart/promocode/set/' . $promocode));
    }
}
