<?php

namespace Happeak\Showcase\Traits;

use Happeak\Showcase\Models\Category;
use Happeak\Showcase\Models\Product;

trait ProductBaseMethods
{

    /**
     * @param Category $category
     * @param Category $subcategory
     * @param Product $product
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product(Category $category, Category $subcategory, Product $product)
    {
        return view('showcase::product.show', [
            'category'    => $category,
            'subcategory' => $subcategory,
            'product'     => $product,
        ]);
    }
}