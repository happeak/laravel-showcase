<?php

namespace Happeak\Showcase\Traits;

use Happeak\Showcase\Filters\Filters\BaseFilter;
use Illuminate\Http\Request;

trait CatalogBaseMethods
{

    /**
     * Search
     *
     * @param Request $request
     * @param BaseFilter $filter
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request, BaseFilter $filter)
    {
        $models = $filter
            ->setParameters([
                'term' => strip_tags($request->query('term')),
            ])
            ->execute();

        return view('showcase::catalog.search', [
            'models' => $models,
        ]);
    }
}