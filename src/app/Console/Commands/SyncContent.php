<?php

namespace Happeak\Showcase\Console\Commands;

class SyncContent extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync command entities';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $banners = $this->happeak->banners->all();

        $this->sync->banners($banners);

        $this->info('synced: banners');

        $contentBlocks = $this->happeak->blocks->all();

        $this->sync->content_blocks($contentBlocks);

        $this->info('synced: content blocks');

        $pages = $this->happeak->pages->all();

        $this->sync->pages($pages);

        $this->info('synced: pages');

        $menus = $this->happeak->menus->all();

        $this->sync->menus($menus);

        $this->info('synced: menus');
    }
}
