<?php

namespace Happeak\Showcase\Console\Commands;

class SyncBrands extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:brands';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация данных брендов';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $brands = $this->happeak->brands->all();

        $this->sync->brands($brands);

        $this->info('Updated: brands');
    }
}
