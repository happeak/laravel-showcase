<?php

namespace Happeak\Showcase\Console\Commands;

use Happeak\Showcase\Services\Synchronizer;
use Happeak\ApiClient;
use Illuminate\Console\Command;

class SyncCommand extends Command
{

    /**
     * @var Synchronizer $sync
     */
    protected $sync;

    /**
     * @var ApiClient $happeak
     */
    protected $happeak;

    public function __construct(ApiClient $happeak, Synchronizer $sync)
    {
        parent::__construct();
        $this->happeak = $happeak;
        $this->sync = $sync;
    }
}