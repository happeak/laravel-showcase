<?php

namespace Happeak\Showcase\Console\Commands;

class SyncCategories extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация списка категорий товаров';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $categories = $this->happeak->categories->all();

        $this->sync->categories($categories);

        $this->info('Updated: categories');
    }
}
