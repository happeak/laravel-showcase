<?php

namespace Happeak\Showcase\Console\Commands;

class SyncProducts extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products {--ids=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация товаров с happeak.ru';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 0;

        do {
            $products = $this->happeak->products->all($offset);
            $offset = $products['last_id'];
            $counter = count($products['products']);

            $this->sync->products($products);
        } while ($counter > 99);

        $this->info('Updated: products');
    }
}
