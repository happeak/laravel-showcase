<?php

namespace Happeak\Showcase\Console\Commands;

class SyncPromotions extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:promotions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация акций';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $promotions = $this->happeak->promotions->filterByCriteria([
            'only_active' => true,
        ]);

        $this->sync->promotions($promotions);

        $this->info('synced: ' . count($promotions['promotions']) . ' promotions');
    }
}
