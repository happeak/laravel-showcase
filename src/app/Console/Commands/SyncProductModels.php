<?php

namespace Happeak\Showcase\Console\Commands;

class SyncProductModels extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:models {--ids=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация данных моделей товаров';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offset = 0;

        do {
            $models = $this->happeak->models->all($offset);
            $offset = $models['last_id'];
            $counter = count($models['models']);

            $this->sync->models($models);
        } while ($counter > 99);

        $this->info('Updated: models');
    }
}
