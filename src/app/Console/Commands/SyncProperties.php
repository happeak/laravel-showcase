<?php

namespace Happeak\Showcase\Console\Commands;

use Happeak\Showcase\Models\Product;
use Happeak\Showcase\Models\ProductModel;

class SyncProperties extends SyncCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:properties {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация справочника свойств товаров';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('type') == 'models') {
            $modelIds = ProductModel::all()->pluck('id')->toArray();

            $options = $this->happeak->options->filterByModelIds($modelIds);

            $this->sync->model_options($options);
            $this->info('synced: model properties');
        }
        if ($this->option('type') == 'products') {
            $productIds = Product::all()->pluck('id')->chunk(100)->toArray();

            foreach ($productIds as $chunk) {
                $options = $this->happeak->options->filterByProductIds($chunk);

                $this->sync->product_options($options);
            }
            $this->info('synced: product properties');
        }
        if (!$this->option('type')) {
            $options = $this->happeak->options->all();
            $this->sync->options($options);
            $this->info('synced: properties dictionary');
        };
    }
}
