<?php

return [

    'ecom' => [
        'client_id'     => env('ECOM_OAUTH_CLIENT_ID'),
        'client_secret' => env('ECOM_OAUTH_CLIENT_SECRET'),
        'redirect'      => env('ECOM_OAUTH_REDIRECT'),
    ],

];
