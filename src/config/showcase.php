<?php

return [
    /*
     * Shop settings. Required to use in API requests.
     */
    'shop_id'               => env('HAPPEAK_SHOP_ID', 1),
    'shop_secret'           => env('HAPPEAK_SHOP_SECRET', ''),

    /*
     * Oauth settings. Required to work with Happeak E-commerce Oauth Service
     */
    'oauth_client_id'       => env('HAPPEAK_OAUTH_ID'),
    'oauth_client_secret'   => env('HAPPEAK_OAUTH_SECRET'),

    /*
     * Pechkin settings. Required to work with Pechkin E-mail Service
     */
    'pechkin_id'            => env('PECHKIN_ID'),
    'pechkin_secret'        => env('PECHKIN_SECRECT'),

    /*
     * Ecom settings.
     */
    'ecom_url'              => env('ECOM_URL', 'https://ecom.happeak.ru'),
    'ecom_slug'             => env('ECOM_SLUG', 'happeak'),

    /*
     * Thumbnails settings
     */
    'thumbnail_service_url' => env('THUMBNAIL_SERVICE_URL', 'https://images.happeak.com'),

    /*
     * Social network links
     */
    'social_vk'             => env('SOCIAL_VK'),
    'social_facebook'       => env('SOCIAL_FACEBOOK'),
    'social_instagram'      => env('SOCIAL_INSTAGRAM'),
];