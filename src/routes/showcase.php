<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace'  => 'Happeak\Showcase\Http\Controllers',
    'middleware' => 'web',
], function () {

    Route::get('promocode/{promocode}', [
        'uses' => 'PromocodeController@setPromocode',
    ]);

    Route::post('subscribe', [
        'uses' => 'SubscribeController@subscribe',
    ]);

    Route::group(['prefix' => 'oauth'], function () {
        Route::group(['prefix' => '{provider}'], function () {
            Route::get('/', [
                'as'   => 'oauth',
                'uses' => 'Auth\OauthController@redirectToProvider',
            ]);
            Route::get('callback', [
                'as'   => 'oauth_callback',
                'uses' => 'Auth\OauthController@handleProviderCallback',
            ]);
        });
    });


    Route::get('{page}', [
        'as'   => 'page',
        'uses' => 'ContentController@show',
    ]);
});