<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_menu_items', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('menu_id');
            $table->unsignedInteger('parent_id')->nullable();

            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ee')->nullable();
            $table->string('name_lv')->nullable();
            $table->string('route')->nullable();
            $table->text('before_content')->nullable();
            $table->text('after_content')->nullable();

            $table->foreign('menu_id')->references('id')->on('shop_menus');
            $table->foreign('parent_id')->references('id')->on('shop_menu_items');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_menu_items');
    }
}
