<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_faq_groups', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('name_en');
            $table->string('name_ee');
            $table->string('name_lv');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('position')->nullable();

            $table->foreign('parent_id')->references('id')->on('shop_faq_groups');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_faq_groups');
    }
}
