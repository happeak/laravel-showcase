<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_faqs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('name_ee')->nullable();
            $table->string('name_lv')->nullable();
            $table->text('answer')->nullable();
            $table->text('answer_en')->nullable();
            $table->text('answer_ee')->nullable();
            $table->text('answer_lv')->nullable();
            $table->unsignedInteger('group_id');
            $table->boolean('is_public')->default(false);
            $table->string('form')->nullable();
            $table->unsignedInteger('position')->nullable();
            $table->string('button_label')->nullable();
            $table->string('button_label_en')->nullable();
            $table->string('button_label_ee')->nullable();
            $table->string('button_label_lv')->nullable();

            $table->foreign('group_id')->references('id')->on('shop_faq_groups');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_faqs');
    }
}
