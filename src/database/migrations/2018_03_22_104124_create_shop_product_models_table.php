<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductModelsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_product_models', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255)->nullable();
            $table->string('name_brand', 255)->nullable();
            $table->string('name_model', 255)->nullable();
            $table->string('name_type', 255)->nullable();
            $table->string('name_type_gen', 255)->nullable();
            $table->string('name_title', 255)->nullable();
            $table->string('name_group', 255)->nullable();

            $table->text('description')->nullable();
            $table->string('slug', 255)->nullable()->unique();
            $table->json('images')->nullable();

            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('subcategory_id')->nullable();

            $table->timestamp('synced_at')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('shop_brands');
            $table->foreign('category_id')->references('id')->on('shop_categories');
            $table->foreign('subcategory_id')->references('id')->on('shop_categories');
        });

        DB::statement('CREATE FULLTEXT INDEX name_description ON shop_product_models(name, description)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_product_models');
    }
}
