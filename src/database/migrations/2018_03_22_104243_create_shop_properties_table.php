<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPropertiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_properties', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->enum('destination', ['model', 'product'])->default('model');
            $table->string('unit', 50)->nullable();
            $table->unsignedInteger('position')->default(0);
            $table->boolean('is_filter')->default(0);
            $table->string('value_type',50)->default('string');
            $table->longText('values')->nullable();
            $table->text('default_value')->nullable();
            $table->string('group', 255)->nullable();

            $table->unsignedInteger('category_id')->nullable();

            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('shop_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_properties');
    }
}
