<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopContentBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_content_blocks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('block');
            $table->longText('content')->nullable();
            $table->longText('content_en')->nullable();
            $table->longText('content_ee')->nullable();
            $table->longText('content_lv')->nullable();
            $table->boolean('is_enabled')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_content_blocks');
    }
}
