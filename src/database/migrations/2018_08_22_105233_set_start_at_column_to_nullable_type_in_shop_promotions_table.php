<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetStartAtColumnToNullableTypeInShopPromotionsTable extends Migration
{

    public function up()
    {
        Schema::table('shop_promotions', function (Blueprint $table) {
            $table->dateTime('start_at')->nullable()->change();
            $table->dateTime('expire_at')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('shop_promotions', function (Blueprint $table) {
            $table->dateTime('start_at')->nullable(false)->change();
            $table->dateTime('expire_at')->nullable(false)->change();
        });
    }
}