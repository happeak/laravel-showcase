<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOnlyAvailableColumnToShopPromotionsTable extends Migration
{

    public function up()
    {
        Schema::table('shop_promotions', function (Blueprint $table) {
            $table->boolean('only_available')->default(false);
        });
    }

    public function down()
    {
        Schema::table('shop_promotions', function (Blueprint $table) {
            $table->dropColumn('only_available');
        });
    }
}