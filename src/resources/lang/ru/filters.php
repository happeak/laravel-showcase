<?php

return [
    'sections' => [
        'category' => [
            'title' => 'Уточните категорию',
        ],
        'price'    => [
            'title'      => 'Цена',
            'price_from' => 'от',
            'price_to'   => 'до',
        ],
        'sizes'    => [
            'title' => 'Размеры',
        ],
        'colors'   => [
            'title' => 'Расцветки',
        ],
        'brands'   => [
            'title' => 'Бренды',
        ],
    ],
];