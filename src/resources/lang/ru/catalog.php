<?php

return [
    'heading'        => 'Каталог',
    'empty'          => 'Товары не найдены',
    'search_results' => 'Результаты поиска по запросу',
    'search'         => 'Поиск',

    'badges' => [
        'promotion' => 'Акция',
    ],

    'filters' => [
        'submit' => 'Применить',
        'reset'  => 'Сбросить',
    ],

    'buttons' => [
        'buy'     => 'Купить',
        'in_cart' => 'В корзине',
    ],
];