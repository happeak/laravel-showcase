<?php

return [
    'delivery'        => 'Delivery',
    'payment'         => 'Payment',
    'return'          => 'Returns & refunds',
    'help'            => 'Help Center',
    'feedback'        => 'Feedback',
    'legal'           => 'User agreement',
    'privacy'         => 'Privacy Policy',
    'information'     => 'Information',
    'login'           => 'Login',
    'register'        => 'Register',
    'forgot_password' => 'Forgot Password?',
    'logout'          => 'Logout',

    'you_have'             => 'You have',
    'you_have_bonus'       => 'bonus',
    'you_have_bonus_title' => 'Actual bonus amount.',

    'account' => [
        'heading'         => 'Account',
        'orders'          => 'My orders',
        'bonus'           => 'Bonus',
        'profile'         => 'Profile',
        'subscriptions'   => 'Subscriptions',
        'addresses'       => 'Delivery addresses',
        'change_password' => 'Change password',
        'invite'          => 'Invite friend',
        'tickets'         => 'My tickets',
    ],
];