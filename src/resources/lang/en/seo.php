<?php

return [
    'catalog' => [
        'category' => [
            'title'       => 'Buy :name in the official web store',
            'description' => ':name of brand :brand. Huge assortment. Delivery to Moscow, Saint-Petersburg and many other regions of Russia.',
            'keywords'    => '',
        ],

        'product' => [
            'title'       => 'Buy :name in the official web store',
            'description' => ':name. Description, photos, reviews, order.',
        ],
    ],
];