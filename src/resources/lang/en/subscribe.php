<?php

return [
    'heading' => 'Newsletter subscription',
    'success' => 'You are successfully subscribed',
];