<?php

return [
    'heading'        => 'Catalog',
    'empty'          => 'Products not found',
    'search_results' => 'Search results for request',
    'search'         => 'Search',

    'badges' => [
        'promotion' => 'Promotion',
    ],

    'filters' => [
        'submit' => 'Submit',
        'reset'  => 'Reset',
    ],

    'buttons' => [
        'buy'     => 'Buy',
        'in_cart' => 'In cart',
    ],
];