<?php

return [
    'sections' => [
        'category' => [
            'title' => 'Select subcategory',
        ],
        'price'    => [
            'title'      => 'Price',
            'price_from' => 'from',
            'price_to'   => 'to',
        ],
        'sizes'    => [
            'title' => 'Size',
        ],
        'colors'   => [
            'title' => 'Color',
        ],
        'brands'   => [
            'title' => 'Brand',
        ],
    ],
];