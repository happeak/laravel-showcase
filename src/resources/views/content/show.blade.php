<?php /** @var \Happeak\Showcase\Models\Page $page */ ?>

@extends('showcase::layouts.base')

@section('title', $page->getMetaTitle())
@section('description', $page->getMetaDescription())
@section('keywords', $page->getMetaKeywords())

@section('content')

    {!! Breadcrumbs::render('page', $page) !!}

    <div class="container pb-3 pt-2">
        <div class="row">
            <div class="col-12">
                <h1>{{ $page->getTitle() }}</h1>

                {!! $page->getContent() !!}
            </div>
        </div>
    </div>

@endsection