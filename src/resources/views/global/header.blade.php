<div class="container-fluid pl-0 pr-0 pre-header">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-center justify-content-between">

                <ul class="list-inline d-none d-md-block">
                    <li class="list-inline-item mr-4">
                        <a href="{{ link_to_ecom('delivery') }}" target="_blank"
                           class="text-underlined">{{ trans('showcase::menu.delivery') }}</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ link_to_ecom('help/feedback') }}" target="_blank"
                           class="text-underlined">{{ trans('showcase::menu.return') }}</a>
                    </li>
                </ul>

                <ul class="list-inline d-flex d-md-block align-items-center justify-content-between">
                    <li class="list-inline-item mr-4">
                        <a href="{{ link_to_ecom('help') }}" target="_blank">
                            {{ trans('showcase::menu.help') }}
                        </a>
                    </li>
                    @if (auth()->check())
                        <li class="list-inline-item mr-4 d-none d-md-inline-block">
                            <span>
                                {{ trans('showcase::menu.you_have') }}
                                <a href="{{ link_to_ecom('account/bonus') }}"
                                   target="_blank"
                                   title="{{ trans('showcase::menu.you_have_bonus_title') }}">
                                    {{ auth()->user()->bonus }} {{ trans('showcase::menu.you_have_bonus') }}
                                </a>
                            </span>
                        </li>
                    @endif
                    <li class="list-inline-item dropdown">
                        @if (auth()->check())
                            <a href="{{ link_to_ecom('account/orders') }}">
                                <i class="fa fa-user-circle-o"></i>
                                {{ auth()->user()->getFullName() }}
                            </a>

                            <ul class="dropdown-menu">
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/orders') }}">{{ trans('showcase::menu.account.orders') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/bonus') }}">{{ trans('showcase::menu.account.bonus') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/profile') }}">{{ trans('showcase::menu.account.profile') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/addresses') }}">{{ trans('showcase::menu.account.addresses') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/subscriptions') }}">
                                        {{ trans('showcase::menu.account.subscriptions') }}
                                    </a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/invites') }}">{{ trans('showcase::menu.account.invite') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/tickets') }}">{{ trans('showcase::menu.account.tickets') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('account/password') }}">{{ trans('showcase::menu.account.change_password') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ route('logout') }}">{{ trans('showcase::menu.logout') }}</a>
                                </li>
                            </ul>
                        @else
                            <a href="{{ route('oauth', ['provider' => 'ecom']) }}">
                                <i class="fa fa-user-circle-o"></i>
                                {{ trans('showcase::menu.account.heading') }}
                            </a>

                            <ul class="dropdown-menu">
                                <li class="dropdown-item">
                                    <a href="{{ route('oauth', ['provider' => 'ecom']) }}">{{ trans('showcase::menu.login') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('register') }}">{{ trans('showcase::menu.register') }}</a>
                                </li>
                                <li class="dropdown-item">
                                    <a href="{{ link_to_ecom('password/reset') }}">{{ trans('showcase::menu.forgot_password') }}</a>
                                </li>
                            </ul>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pl-0 pr-0 pt-3 pb-3 header">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-center justify-content-between">
                <a href="{{ route('homepage') }}">
                    <img src="{{ file_exists(public_path('images/logo.svg')) ? asset('images/logo.svg') : asset('images/logo.png')}}"
                         alt="" class="logo">
                </a>

                <ul class="list-inline menu mb-0 d-none d-lg-block">
                    <li class="list-inline">
                        <a href=""></a>
                    </li>
                </ul>

                @include('showcase::global.search', ['class' => 'd-none d-md-block'])

                <ul class="list-inline mb-0 ml-auto ml-md-0 mr-3">
                    <li class="list-inline-item">
                        <cart-widget></cart-widget>
                    </li>
                </ul>

                <a href="#mobile-menu" class="d-inline d-md-none">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
    </div>
</div>