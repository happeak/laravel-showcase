@if (session()->has('success'))
    <div class="alert mt-3 alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if (session()->has('error'))
    <div class="alert mt-3 alert-danger">
        {{ session()->get('error') }}
    </div>
@endif
@if (session()->has('warning'))
    <div class="alert mt-3 alert-warning">
        {{ session()->get('warning') }}
    </div>
@endif