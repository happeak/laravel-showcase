<div class="container-fluid pl-0 pr-0 pb-3 pt-3 footer-ecommerce">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center text-md-left d-flex flex-column flex-md-row align-items-center justify-content-between">
                <img src="{{ url('vendor/showcase/images/logo_ecommerce.svg') }}" class="logo mb-3 mb-md-0">
                <ul class="list-inline">
                    <li class="list-inline-item mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('help') }}" target="_blank">
                            {{ trans('showcase::menu.help') }}
                        </a>
                    </li>
                    <li class="list-inline-item mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('legal') }}"
                           target="_blank">
                            {{ trans('showcase::menu.legal') }}
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ link_to_ecom('legal') }}#privacy"
                           target="_blank">
                            {{ trans('showcase::menu.privacy') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>