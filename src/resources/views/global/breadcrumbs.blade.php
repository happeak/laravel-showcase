@if (count($breadcrumbs))

    <div class="container">
        <div class="row">
            <div class="col-12">
                <ol class="breadcrumb pb-0">
                    @foreach ($breadcrumbs as $breadcrumb)

                        @if ($breadcrumb->url && !$loop->last)
                            <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                        @else
                            <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
                        @endif

                    @endforeach
                </ol>
            </div>
        </div>
    </div>

@endif
