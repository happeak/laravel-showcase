<form action="{{ route('search') }}" method="get" class="form-search @isset($class) {{ $class }} @endisset">
    <div class="input-group">
        <input type="text" class="form-control" name="term" placeholder="{{ trans('showcase::catalog.search') }}..."
               value="{{ request('term') ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn-outline-black" role="button">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</form>