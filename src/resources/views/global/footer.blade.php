<div class="container-fluid pl-0 pr-0 pb-3 pt-4 footer bg-grey border-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 d-flex align-items-center flex-column justify-content-center">
                <a href="{{ route('homepage') }}" class="d-none d-md-inline mb-3">
                    <img src="{{ file_exists(public_path('images/logo.svg')) ? asset('images/logo.svg') : asset('images/logo.png')}}"
                         alt="" class="logo img-fluid">
                </a>

                <ul class="list-inline mb-3 mb-md-0 social d-none d-md-block">
                    <li class="list-inline-item">
                        <a href="{{ config('showcase.social_vk') }}" class="vk"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ config('showcase.social_facebook') }}" class="facebook"></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ config('showcase.social_instagram') }}" class="instagram"></a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-3 text-center text-md-left">
                <p class="lead"><strong>{{ trans('showcase::menu.information') }}</strong></p>

                <ul class="list-unstyled mb-3 mb-md-0">
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('delivery') }}">{{ trans('showcase::menu.delivery') }}</a>
                    </li>
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('delivery') }}">{{ trans('showcase::menu.payment') }}</a>
                    </li>
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('help/feedback') }}">{{ trans('showcase::menu.return') }}</a>
                    </li>
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('help/feedback') }}">{{ trans('showcase::menu.help') }}</a>
                    </li>
                    <li>
                        <a href="{{ link_to_ecom('help/feedback') }}">{{ trans('showcase::menu.feedback') }}</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-3 text-center text-md-left">
                <p class="lead"><strong>{{ trans('showcase::menu.account.heading') }}</strong></p>

                <ul class="list-unstyled mb-3 mb-md-0">
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('account/orders') }}">{{ trans('showcase::menu.account.orders') }}</a>
                    </li>
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('account/bonus') }}">{{ trans('showcase::menu.account.bonus') }}</a>
                    </li>
                    <li class="mb-2 mb-md-0">
                        <a href="{{ link_to_ecom('account/profile') }}">{{ trans('showcase::menu.account.profile') }}</a>
                    </li>
                    <li>
                        <a href="{{ link_to_ecom('account/subscriptions') }}">{{ trans('showcase::menu.account.subscriptions') }}</a>
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-3 text-center text-md-left">
                <p class="lead"><strong>{{ trans('showcase::subscribe.heading') }}</strong></p>

                <form-subscribe></form-subscribe>
            </div>
        </div>
    </div>
</div>

@include('showcase::global.footer_ecommerce')

<nav id="mobile-menu">
    <ul class="list-unstyled">
        <li><a href=""></a></li>
    </ul>
</nav>