@if (isset($category))
    <div class="modal fade" tabindex="-1" role="dialog" id="modalMatchTable" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="table-responsive text-center">
                        <h3>Таблица соответствий размеров</h3>
                        @if ($category->id === 373)
                            <table class="table table-bordered table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>Размер (Russian)</th>
                                    <th>Размер (International)</th>
                                    <th>Обхват груди, см</th>
                                    <th>Обхват талии, см</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>44/46</td>
                                    <td>XS</td>
                                    <td>86-94</td>
                                    <td>74-82</td>
                                </tr>
                                <tr>
                                    <td>46/48</td>
                                    <td>S</td>
                                    <td>90-98</td>
                                    <td>78-86</td>
                                </tr>
                                <tr>
                                    <td>48/50</td>
                                    <td>M</td>
                                    <td>94-102</td>
                                    <td>82-90</td>
                                </tr>
                                <tr>
                                    <td>50/52</td>
                                    <td>L</td>
                                    <td>98-106</td>
                                    <td>86-94</td>
                                </tr>
                                <tr>
                                    <td>52/54</td>
                                    <td>XL</td>
                                    <td>102-110</td>
                                    <td>90-98</td>
                                </tr>
                                <tr>
                                    <td>54/56</td>
                                    <td>XXL</td>
                                    <td>106-114</td>
                                    <td>94-102</td>
                                </tr>
                                <tr>
                                    <td>56/58</td>
                                    <td>3XL</td>
                                    <td>110-118</td>
                                    <td>98-106</td>
                                </tr>
                                <tr>
                                    <td>58/60</td>
                                    <td>4XL</td>
                                    <td>114-122</td>
                                    <td>102-110</td>
                                </tr>
                                <tr>
                                    <td>62/64</td>
                                    <td>5XL</td>
                                    <td>122-130</td>
                                    <td>110-118</td>
                                </tr>
                                </tbody>
                            </table>
                        @elseif ($category->id === 371)
                            <table class="table table-bordered table-hover mb-0">
                                <thead>
                                <tr>
                                    <th class="align-middle">
                                        Российский размер
                                    </th>

                                    <th class="align-middle">
                                        Размер производителя
                                    </th>

                                    <th class="align-middle">
                                        Длина стопы (см)
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        37
                                    </td>
                                    <td>
                                        5,5
                                    </td>
                                    <td>
                                        23,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        37,5
                                    </td>
                                    <td>
                                        6
                                    </td>
                                    <td>
                                        24
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        38
                                    </td>
                                    <td>
                                        6,5
                                    </td>
                                    <td>
                                        24,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        39
                                    </td>
                                    <td>
                                        7
                                    </td>
                                    <td>
                                        25
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        39,5
                                    </td>
                                    <td>
                                        7,5
                                    </td>
                                    <td>
                                        25,3
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        40
                                    </td>
                                    <td>
                                        8
                                    </td>
                                    <td>
                                        25,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        41
                                    </td>
                                    <td>
                                        8,5
                                    </td>
                                    <td>
                                        26,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        41,5
                                    </td>
                                    <td>
                                        9
                                    </td>
                                    <td>
                                        26,7
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        42
                                    </td>
                                    <td>
                                        9,5
                                    </td>
                                    <td>
                                        27
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        43
                                    </td>
                                    <td>
                                        10
                                    </td>
                                    <td>
                                        27,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        43,5
                                    </td>
                                    <td>
                                        10,5
                                    </td>
                                    <td>
                                        28
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        44
                                    </td>
                                    <td>
                                        11
                                    </td>
                                    <td>
                                        28,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        44,5
                                    </td>
                                    <td>
                                        11,5
                                    </td>
                                    <td>
                                        28,7
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        45
                                    </td>
                                    <td>
                                        12
                                    </td>
                                    <td>
                                        29
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        46
                                    </td>
                                    <td>
                                        12,5
                                    </td>
                                    <td>
                                        29,5
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        46,5
                                    </td>
                                    <td>
                                        13
                                    </td>
                                    <td>
                                        30
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        47
                                    </td>
                                    <td>
                                        13,5
                                    </td>
                                    <td>
                                        30,5
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif