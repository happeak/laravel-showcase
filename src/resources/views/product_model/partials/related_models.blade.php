@if (count($relatedModels) > 0)

    <h2 class="h3">Похожие товары</h2>

    <div class="row">
        @each('showcase::catalog.partials.tile', $relatedModels, 'model')
    </div>

@endif