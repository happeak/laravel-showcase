<?php
/**
 * @var \Happeak\Showcase\Models\Category $category
 * @var \Happeak\Showcase\Models\Category $subcategory
 * @var \Happeak\Showcase\Models\ProductModel $productModel
 * @var \Happeak\Showcase\Models\Product $product
 */
?>

@extends('showcase::layouts.base')

@section('title', sprintf('Купить %s в официальном интернет-магазине Kelme.', mb_strtolower($productModel->getName())))
@section('description', sprintf('%s. Описание, отзывы, оформление заказа.', $productModel->getName()))

@section('content')

    {!! Breadcrumbs::render('product_model', $category, $subcategory, $productModel) !!}

    <main class="container pt-3 pb-3">
        <div class="row mb-5">
            <div class="col-12 col-md-8">
                <div class="row">
                    <div class="col-12 col-md-8">
                        @if ($product->getPreview())
                            <div class="product-image-container d-flex justify-content-center">
                                @if ($product->hasPromotion())
                                    <div class="ribbons">
                                        <div class="ribbon">Акция!</div>
                                    </div>
                                @endif
                                <img src="{{ thumbnail($product->getPreview(), 'happeak_product_360x440') }}" alt=""
                                     class="img-fluid">
                            </div>
                        @endif
                    </div>
                    <div class="col-12 col-md-4 d-flex flex-row flex-lg-column mb-3 product-small-image-container">
                        @if ($product->hasImages())
                            @foreach ($product->getImages() as $photo)
                                <div class="p-1 d-flex align-items-center justify-content-center border-black">
                                    <a href="{{ thumbnail($photo, 'happeak_product_360x440') }}" class="product-thumbnail">
                                        <img src="{{ thumbnail($photo, 'happeak_product_80x80') }}" alt=""
                                             class="img-fluid">
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <h1 class="h2">{{ $productModel->getName() }}</h1>
                <p class="lead">
                    <a href="{{ route('catalog_subcategory', ['category' => $category, 'subcategory' => $subcategory]) }}">
                        {{ $subcategory->getName() }}
                    </a>
                </p>

                @if ($product->hasDiscount())
                    <p class="price old mb-0">
                        {{ price($product->getBasePrice()) }}
                    </p>
                @endif

                <p class="lead price">
                    <strong>{{ price($product->getPrice()) }}</strong>
                </p>

                <hr class="mb-4">

                <cart-button></cart-button>
            </div>
        </div>

        @if ($product->getDescription())
            <div class="row mb-5">
                <div class="col-12">
                    <h2 class="h3">Описание</h2>

                    {!! $product->getDescription() !!}
                </div>
            </div>
        @endif

        @include('showcase::product_model.partials.related_models')
    </main>

@endsection