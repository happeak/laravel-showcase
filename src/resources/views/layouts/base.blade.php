@extends('showcase::layouts.html')

@section('assets_top')
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{ link_to_ecom('js/cart.js') }}"></script>
@endsection

@section('body')
    <div id="app">

        @include('showcase::global.header')

        @yield('content')

        @include('showcase::global.footer')

    </div>
@endsection