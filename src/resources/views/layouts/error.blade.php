@extends('showcase::layouts.html')

@section('assets_top')
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
@endsection

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="error d-flex align-items-center justify-content-center flex-column">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
@endsection