@extends('showcase::layouts.base')

@section('title', trans('showcase::seo.catalog.category.title', ['name' => isset($subcategory) ? $subcategory->getName() : $category->getName()]))
@section('title', trans('showcase::seo.catalog.category.description', ['name' => isset($subcategory) ? $subcategory->getName() : $category->getName()]))
@section('description', trans('showcase::seo.catalog.category.keywords', ['name' => isset($subcategory) ? $subcategory->getName() : $category->getName()]))

@section('content')

    {{--@if ($category->parent_id)--}}
    {{--{!! Breadcrumbs::render('subcategory', $category->parent, $category) !!}--}}
    {{--@else--}}
    {{--{!! Breadcrumbs::render('category', $category) !!}--}}
    {{--@endif--}}

    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('showcase::global.flashes')
            </div>
        </div>
    </div>

    <main class="container pt-3 pb-3">
        <div class="row">
            <div class="col-12 col-md-3">
                <h2 class="h1 d-none d-md-block"><strong>{{ trans('showcase::catalog.heading') }}</strong></h2>
                <h2 class="h1 d-block d-md-none">
                    <strong>{{ isset($subcategory) ? $subcategory->getName() : $category->getName() }}</strong></h2>
                <hr>
            </div>
            <div class="col-12 col-md-9">
                <h1 class="d-none d-md-block">
                    <strong>{{ isset($subcategory) ? $subcategory->getName() : $category->getName() }}</strong></h1>
                <hr class="d-none d-md-block">

                @if (count($models) > 0)
                    <div class="row">
                        @each('showcase::catalog.partials.tile', $models, 'model')
                    </div>

                    <div class="d-flex justify-content-center">
                        {{ $models->links() }}
                    </div>
                @else
                    {{ trans('showcase::catalog.empty') }}
                @endif
            </div>
        </div>
    </main>

@endsection