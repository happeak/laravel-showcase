<?php
/**
 * @var \Happeak\Showcase\Models\ProductModel $model
 */

$product = $model->getFirstProduct();
?>

<div class="col-6 col-md-4 col-lg-3 text-center">
    <div class="product-tile text-center">
        <div class="product-image mb-3">
            @if ($model->hasPromotion())
                <div class="ribbons">
                    <div class="ribbon">{{ trans('showcase::catalog.badges.promotion') }}!</div>
                </div>
            @endif
            <a href="{{ link_to_product($product) }}">
                <img src="{{ thumbnail($model->getPreview()) }}"
                     alt="{{ $model->getName() }}"
                     class="img-fluid">
            </a>
        </div>
        <div class="product-name">
            <p class="mb-0">{{ $model->getNameModel() }}</p>
        </div>
        <p class="lead"><strong class="price">{{ price($model->getPrice()) }}</strong></p>
        <a href="{{ link_to_product($product) }}"
           class="btn btn-outline-black">
            {{ trans('showcase::catalog.buttons.buy') }}
        </a>
    </div>
</div>