@extends('showcase::layouts.base')

@section('content')

    {!! Breadcrumbs::render('search', request('term')) !!}

    <main class="container pt-3 pb-3">
        <div class="row">
            <div class="col-12 col-md-3">
                <h2 class="h1"><strong>{{ trans('showcase::catalog.heading') }}</strong></h2>
                <hr>
                @isset($filterForm)
                    {!! $filterForm->render() !!}
                @endisset
            </div>
            <div class="col-12 col-md-9">
                <h1><strong>{{ trans('showcase::catalog.search_results') }} "{{ request('term') }}"</strong></h1>
                <hr>

                @if (count($models) > 0)
                    <div class="row">
                        @each('showcase::catalog.partials.tile', $models, 'model')
                    </div>
                @else
                    {{ trans('showcase::catalog.empty') }}
                @endif
            </div>
        </div>
    </main>

@endsection