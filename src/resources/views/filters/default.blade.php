<?php
/**
 * @var \Happeak\Showcase\Filters\FilterForms\BaseFilterForm $filterForm
 * @var \Happeak\Showcase\Filters\FilterForms\Sections\BaseFilterFormSection $section
 */
?>

@if ($filterForm->isNotEmpty())
    <form action="" method="get">
        @foreach ($filterForm->getSections() as $section)
            {!! $section->render() !!}
        @endforeach

        <div class="row">
            <div class="col-6">
                <button class="btn btn-outline-black" type="submit">
                    {{ trans('showcase::catalog.filters.submit') }}
                </button>
            </div>
            <div class="col-6 text-right">
                <a href="{{ ($subcategory = request() ->route()->subcategory) ? link_to_subcategory($subcategory) : link_to_category(request()->route()->category) }}"
                   class="btn btn-outline-red">
                    {{ trans('showcase::catalog.filters.reset') }}
                </a>
            </div>
        </div>
    </form>
@endif