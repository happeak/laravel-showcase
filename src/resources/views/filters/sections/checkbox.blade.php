<?php
/**
 * @var \Happeak\Showcase\Filters\FilterForms\Sections\BaseFilterFormSection $section
 */
?>

@if (!empty($section->getData()))
    <p class="lead"><strong>{{ $section->getTitle() }}</strong></p>
    <div class="row no-gutters">
        @foreach ($section->getData() as $value => $label)
            <div class="col-6">
                <div class="form-check">
                    <input type="checkbox"
                           @if (request($section->getName()) && in_array($value, request($section->getName())))
                           checked
                           @endif
                           name="{{ $section->getName() }}[]"
                           id="{{ $section->getName() }}-{{ $loop->index }}"
                           value="{{ $value }}"
                           class="form-check-input">
                    <label for="{{ $section->getName() }}-{{ $loop->index }}" class="form-check-label">
                        {{ $label }}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
    <hr>
@endif