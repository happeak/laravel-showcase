<?php
/**
 * @var \Happeak\Showcase\Filters\FilterForms\Sections\InputSection $section
 */
?>

<p class="lead"><strong>{{ $section->getTitle() }}</strong></p>

<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="{{ $section->getName() }}" class="sr-only"></label>
            <input type="text" name="{{ $section->getName() }}" id="{{ $section->getName() }}"
                   value="{{ request($section->getName()) ?? $section->getDefault($section->getName()) }}"
                   class="form-control border-left-0">
        </div>
    </div>
</div>
<hr>