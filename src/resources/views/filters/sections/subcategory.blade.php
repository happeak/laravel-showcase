<?php
/**
 * @var \Happeak\Showcase\Filters\FilterForms\Sections\SubcategorySection $section
 */
?>

@if (!empty(($section->getData()) && array_key_exists('subcategories', $section->getData())))
    <p class="lead"><strong>{{ $section->getTitle() }}</strong></p>

    <ul class="list-unstyled mb-0">
        @foreach ($section->getData()['subcategories'] as $subcategory)
            <li>
                <a href="{{ link_to_subcategory($subcategory) }}">
                    {{ $subcategory->getName() }}
                </a>
            </li>
        @endforeach
    </ul>

    <hr>
@endif
