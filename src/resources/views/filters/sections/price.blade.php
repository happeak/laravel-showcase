<?php
/**
 * @var \Happeak\Showcase\Filters\FilterForms\Sections\BaseFilterFormSection $section
 */
?>

<p class="lead"><strong>{{ $section->getTitle() }}</strong></p>
<div class="form-row">
    <div class="col">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text border-right-0">{{ trans('showcase::filters.sections.price.price_from') }}</span>
            </div>
            <label for="price_from" class="sr-only"></label>
            <input type="text" name="price_from" id="price_from"
                   value="{{ request('price_from') ?? intval($section->getDefault('price_from')) }}"
                   class="form-control border-left-0">
        </div>
    </div>
    <div class="col">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text border-right-0">{{ trans('showcase::filters.sections.price.price_to') }}</span>
            </div>
            <label for="price_to" class="sr-only"></label>
            <input type="text" name="price_to" id="price_to"
                   value="{{ request('price_to') ?? intval($section->getDefault('price_to')) }}"
                   class="form-control border-left-0">
        </div>
    </div>
</div>

<hr>