<?php
/**
 * @var \Happeak\Showcase\Models\SiteMenu $menu
 * @var \Happeak\Showcase\Models\SiteMenuItem $item
 */
?>

@if ($menu->hasItems())
    <ul class="list-inline d-flex mb-0 align-items-center justify-content-between menu-categories">
        @foreach ($menu->getItems() as $item)
            <li class="list-inline-item">
                <a href="{{ url($item->getRoute()) }}">{{ $item->getName() }}</a>
            </li>
        @endforeach
    </ul>
@endif