<?php
/**
 * @var \Happeak\Showcase\Models\SiteMenu $menu
 * @var \Happeak\Showcase\Models\SiteMenuItem $item
 */
?>

@if ($menu->hasItems())
    <ul class="list-inline mb-0">
        @foreach ($menu->getItems() as $item)
            <li class="list-inline-item">
                <a href="{{ url($item->getRoute()) }}">{{ $item->getName() }}</a>
            </li>
        @endforeach
    </ul>
@endif