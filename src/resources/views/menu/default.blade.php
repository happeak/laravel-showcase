<?php
/**
 * @var \Happeak\Showcase\Models\SiteMenu $menu
 * @var \Happeak\Showcase\Models\SiteMenuItem $item
 */
?>

@if ($menu->hasItems())
    <ul class="list-unstyled">
        @foreach ($menu->getItems() as $item)
            <li><a href="{{ url($item->getRoute()) }}">{{ $item->getName() }}</a></li>
        @endforeach
    </ul>
@endif